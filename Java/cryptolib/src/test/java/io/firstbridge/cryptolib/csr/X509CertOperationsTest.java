/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.cryptolib.csr;

import io.firstbridge.cryptolib.CryptoConfig;
import io.firstbridge.cryptolib.CryptoParams;
import io.firstbridge.cryptolib.KeyGenerator;
import io.firstbridge.cryptolib.KeyWriter;
import io.firstbridge.cryptolib.impl.KeyWriterImpl;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import io.firstbridge.cryptolib.impl.csr.X509CertOperationsImpl;
import io.firstbridge.cryptolib.impl.ecc.KeyGeneratorEC;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author alukin@gmail.com
 */
public class X509CertOperationsTest {
 
    private static CryptoParams params = CryptoConfig.createDefaultParams();   
    
    private static final String CERT_FILE="../../testdata/certops_test_cert.pem";
    private static final String CSR_FILE="../../testdata/certops_test_csr.pem";
  
    
    public X509CertOperationsTest() {
    }
    
    private static Properties fillProperties() {
        Properties p = new Properties();
        p.put("subject.CN", "al@cn.ua");
        p.put("subject.O", "FirstBridge");
        p.put("subject.OU", "FB-cn");
        p.put("subject.L", "Chernigiv");
        p.put("subject.C", "UA");
        p.put("subject.emailAddress", "a.lukin@gmail.com");
        p.put("subject.SERIALNUMBER", "1234567890");
        p.put("subject.UID", "1234567890");
        return p;
    }
  

    /**
     * Test of createSelfSignedX509v3 method, of class X509CertOperations.
     */
    @Test
    public void testCreateSelfSignedX509v3() throws Exception {
        System.out.println("createSelf SignedX509v3");
        Properties p = fillProperties();
        CertificateRequestData certData = CertificateRequestData.fromProperty(p, CertificateRequestData.CSRType.PERSON);
        certData.processCertData(true);
        KeyGenerator kg = new KeyGeneratorEC(params);
        KeyPair kp = kg.generateKeys();
        
        X509CertOperations instance = new X509CertOperationsImpl(params);
        X509Certificate result = instance.createSelfSignedX509v3(kp, certData);
        System.out.println(result.getIssuerDN().toString());
        Map<String,String> subjAtrributes = CertSubject.byNamesFromPrincipal(result.getIssuerDN());
        assertEquals("FirstBridge", subjAtrributes.get("O"));
        assertEquals("a.lukin@gmail.com", subjAtrributes.get("E"));
        KeyWriter kw = new KeyWriterImpl();
        kw.writeX509CertificatePEM(CERT_FILE, result);
    }

    /**
     * Test of createX509CertificateRequest method, of class X509CertOperations.
     */
    @Test
    public void testCreateX509CertificateRequest() throws Exception {
        System.out.println("createX509CertificateRequest");
        Properties p = fillProperties();
        CertificateRequestData certData = CertificateRequestData.fromProperty(p, CertificateRequestData.CSRType.PERSON);
        certData.processCertData(true);
        KeyGenerator kg = new KeyGeneratorEC(params);
        KeyPair kp = kg.generateKeys();
        
        boolean allowCertSign = false;
        String challengePassword = "1234567890";
        
        X509CertOperations instance = new X509CertOperationsImpl(params);
        PKCS10CertificationRequest result = instance.createX509CertificateRequest(kp, certData, allowCertSign, challengePassword);
        System.out.println(result.getSubject().toString());
        Map<String,String> subjAtrributes = CertSubject.byNames(result.getSubject());
        assertEquals("FirstBridge", subjAtrributes.get("O"));
        assertEquals("a.lukin@gmail.com", subjAtrributes.get("E"));
        KeyWriter kw = new KeyWriterImpl();
        kw.writeCertificateRequestPEM(CSR_FILE, result);
    }


}
