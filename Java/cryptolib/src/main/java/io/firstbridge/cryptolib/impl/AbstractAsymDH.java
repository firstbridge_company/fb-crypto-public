/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

package io.firstbridge.cryptolib.impl;

import io.firstbridge.cryptolib.CryptoParams;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Objects;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.firstbridge.cryptolib.AsymCryptorDH;
import io.firstbridge.cryptolib.AsymKeysHolder;
import io.firstbridge.cryptolib.CryptoSignature;

/**
  *
  * @author Serhiy Lymar serhiy.lymar@gmail.com 
  */
public abstract class AbstractAsymDH implements AsymCryptorDH {
    private static final Logger log = LoggerFactory.getLogger(AbstractAsymDH.class);

    protected Cipher blockCipherAsym;
    protected GCMParameterSpec gcmParameterSpecAsym;
    protected PrivateKey privateKey;
    protected PublicKey ourPublicKey;
    protected PublicKey theirPublicKey;
    protected SecretKeySpec sharedKey;
    protected KeyPair ephemeralKeys;            
    protected final CryptoParams params;
    protected final CryptoSignature signer;
    
     public AbstractAsymDH(CryptoParams params) {
        this.params = Objects.requireNonNull(params);
        signer = new CryptoSignatureImpl(params);
    } 
    
    public CryptoParams getParams(){
        return params;
    }
    
    /**
     * Set all required keys
     * @param keys our public and private keys, their public key
     */
    @Override
    public void setKeys(AsymKeysHolder keys){
        this.ourPublicKey = keys.getOurPublicKey();
        this.privateKey = keys.getPrivateKey();
        this.theirPublicKey = keys.getTheirPublicKey();
        this.signer.setKeys(keys);
        try {
            blockCipherAsym = Cipher.getInstance(params.getAsymCipher());
            calculateSharedKey();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            log.error("Can not create cipher for {} :", params.getAsymCipher(), ex);
        }        
    }    
    
    protected abstract byte[] doCalculateShared(PublicKey ourPub, PrivateKey ourPriv, PublicKey theirPub) throws NoSuchAlgorithmException, InvalidKeyException;
    
}
