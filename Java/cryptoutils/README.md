## Utility to generate CSR fro X.509 v.3

##Compilation

You need JDK 8 or later and Maven 3.5.0 or later
Simple command will do the job
<pre>
mvn install
</pre>

##Example

### Generate self-signed X.509 certificate using template
 
fbcryptoutil certreq --selfsigned -r softsign -t SELFSIGNED_Apollo.properties -i