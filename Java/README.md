## Java implementation of FBCrypto and generation CSR utility

### Components
 * cryptolib - Crypto librarry
 * cryptoutils - utility to work with CSR, Cetrtificates, kesy, etc
 * cryptolib-examples Simple examples of library usage for symmetric and asymmetric cryptography

### Usage

Examples of library usage are in cryptolib-examples subproject


Please see tests and cryptoutils module for sdditional examples

### Compilation

You need JDK 8 or later and Maven 3.6.0 or later.

Simple command will do the job

<pre>
mvn install
</pre>

Generating javadoc API documentation into docs directory

<pre>
cd cryptolib
mvn javadoc:javadoc
</pre>