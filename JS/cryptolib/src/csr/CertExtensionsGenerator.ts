const { BasicConstraints, Extension } = require('pkijs');

export enum SupportedExtensions {
  KEYPURPOSE,
  SUBJALTNAMES,
  SUBJATTR,
}

export enum KeyPurposeValue {
  HOST,
  PERSONAL,
  SOFTSIGN,
}

type KeyPurposeType = keyof typeof KeyPurposeValue;

export enum SupportedAltNames {
  IPADDRESS,
  DNSNAME,
  DIRECTORYNAME,
  REGISTEREDID,
  OTHERNAME,
  EMAIL,
  EDIPARTYNAME,
  UNIFORMRESQOURCEID,
}

/**
 * X.500 is total big crap and we need just selected attributes for X.509v3
 * certificates but we need it in ASN1/DER format... so This class creates
 * attributes by names and values
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class CertExtensionsGenerator {
  private _extensions: any[] = [];

  createDefaultExtensions(allowCertSign: boolean): void {
    const basicConstr = new BasicConstraints({
      cA: allowCertSign,
      pathLenConstraint: 3,
    });

    this.extensions.push(
      new Extension({
        extnID: '2.5.29.19',
        critical: false,
        extnValue: basicConstr.toSchema().toBER(false),
        parsedValue: basicConstr, // Parsed value for well-known extensions
      }),
    );
  }

  get extensions(): any[] {
    return this._extensions;
  }

  set extensions(value: any[]) {
    this._extensions = value;
  }
}
