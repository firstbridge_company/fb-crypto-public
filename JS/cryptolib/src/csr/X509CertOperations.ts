/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CertificateRequestData from './CertificateRequestData';
import CertificationRequest from 'pkijs/src/CertificationRequest';
import Certificate from 'pkijs/src/Certificate';

/**
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface X509CertOperations {
  createSelfSignedX509v3(
    kp: CryptoKeyPair,
    certData: CertificateRequestData,
  ): Promise<{
    ['certificate']: Certificate;
    ['certificatePem']: string;
    ['privateKeyPem']: string;
    ['publicKeyPem']: string;
  }>;

  createX509CertificateRequest(
    kp: CryptoKeyPair,
    certData: CertificateRequestData,
    challengePassword: string,
  ): Promise<{
    ['certificateRequest']: CertificationRequest;
    ['certificateRequestPem']: string;
    ['privateKeyPem']: string;
    ['publicKeyPem']: string;
  }>;

  verifyCertificate(
    trustedCertificates: Certificate[],
    certificates: Certificate[],
    crls: Certificate[],
  ): Promise<boolean>;
}
