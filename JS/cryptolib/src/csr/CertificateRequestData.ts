/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import * as asn1js from 'asn1js';
import CertificationRequest from 'pkijs/src/CertificationRequest';
const { AttributeTypeAndValue } = require('pkijs');
import CertExtensionsGenerator from './CertExtensionsGenerator';

export interface SubjectProperties {
  CN?: string;
  C?: string;
  O?: string;
  T?: string;
  OU?: string;
  L?: string;
  ST?: string;
  SERIALNUMBER?: string;
  E?: string;
  DC?: string;
  UID?: string;
  STREET?: string;
  SURNAME?: string;
  GIVENNAME?: string;
  INITIALS?: string;
  GENERATION?: string;
  unstructuredAddress?: string;
  unstructuredName?: string;
  UniqueIdentifier?: string;
  DN?: string;
  Pseudonym?: string;
  PostalAddress?: string;
  NameAtBirth?: string;
  CountryOfCitizenship?: string;
  CountryOfResidence?: string;
  Gender?: string;
  PlaceOfBirth?: string;
  DateOfBirth?: string;
  PostalCode?: string;
  BusinessCategory?: string;
  TelephoneNumber?: string;
  Name?: string;
  organizationIdentifier?: string;
}

export enum CSRType {
  HOST,
  PERSON,
  SOFTSIGN,
}

/**
 * REFERENCES
 * https://tools.ietf.org/html/rfc2986 PKCS #10: Certification Request Syntax Specification
 *                                      Version 1.7
 */
export default class CertificateRequestData {
  private attrGen?: CertExtensionsGenerator;
  private _extensions: any[] = [];
  public static CSRType = CSRType;
  public sa: any = {
    subject: {},
    attributes: {},
  };
  public certDataMap: any = {
    subject: {},
    attributes: {},
  };

  constructor(type: CSRType) {
    this.sa.subject = CertificateRequestData.getSupportedSubjectAttributes();
    this.setCSRType(type);
  }

  set extensions(value: any[]) {
    this._extensions = value;
  }

  get extensions(): any[] {
    return this._extensions;
    // const extensions: any[] = [];
    // Object.keys(this.certDataMap.attributes).map((key: string) => {
    //   if (this.certDataMap.attributes[key] && this.sa.attributes[key]) {
    //     extensions.push(new Extension({
    //       extnID: this.sa.attributes[key],
    //       critical: false,
    //       extnValue: this.certDataMap.attributes[key].toSchema().toBER(false),
    //       parsedValue: this.certDataMap.attributes[key] // Parsed value for well-known extensions
    //     }))
    //   }
    // });
    // return extensions;
  }

  get subject(): any[] {
    const subject: any[] = [];
    Object.keys(this.certDataMap.subject).map((key: string) => {
      if (this.certDataMap.subject[key] && this.sa.subject[key]) {
        subject.push(
          new AttributeTypeAndValue({
            type: this.sa.subject[key],
            value: new asn1js.PrintableString({ value: this.certDataMap.subject[key] }),
          }),
        );
      }
    });
    return subject;
  }

  public processCertData(allowCertSign: boolean): void {
    this.attrGen = new CertExtensionsGenerator();

    try {
      this.attrGen.createDefaultExtensions(allowCertSign);
      this.extensions = this.attrGen.extensions;
    } catch (ex) {
      console.error(`Exception inside of extension generator\n${ex}`);
    }
  }

  public static fromMap(cd: SubjectProperties, type: CSRType): CertificateRequestData {
    const res: CertificateRequestData = new CertificateRequestData(type);
    res.certDataMap.subject = {
      ...res.certDataMap.subject,
      ...cd,
    };
    return res;
  }

  public static fromProperty(p: SubjectProperties, type: CSRType): CertificateRequestData {
    return this.fromMap(p, type);
  }

  public setCSRType(type: CSRType): void {
    switch (type) {
      case CSRType.HOST:
        {
          this.emptyParamMapForHost();
        }
        break;
      case CSRType.PERSON:
        {
          this.emptyParamMapForPerson();
        }
        break;
      case CSRType.SOFTSIGN: {
        this.emptyParamMapForSoftSigner();
      }
    }
  }

  public emptyParamMapForHost(): void {
    this.certDataMap = {
      subject: {},
      attributes: {},
    };
    this.certDataMap.subject['CN'] = ''; // Canonical name
    this.certDataMap.subject['O'] = ''; // Organization
    this.certDataMap.subject['OU'] = ''; // Organizational unit
    this.certDataMap.subject['L'] = ''; // Locality name (e.g. city)
    this.certDataMap.subject['C'] = ''; // 2-char country code
    this.certDataMap.subject['E'] = '';
    this.certDataMap.subject['UID'] = ''; // LDAP user Id or entity Id
    this.certDataMap.attributes['KEYPURPOSE'] = 'host'; // This attribute is fixed for cert type
  }

  public emptyParamMapForPerson(): void {
    this.certDataMap = {
      subject: {},
      attributes: {},
    };
    this.certDataMap.subject['CN'] = ''; // Canonical name
    this.certDataMap.subject['O'] = ''; // Organization
    this.certDataMap.subject['OU'] = ''; // Organizational unit
    this.certDataMap.subject['L'] = ''; // Locality name (e.g. city)
    this.certDataMap.subject['C'] = ''; // 2-char country code
    this.certDataMap.subject['E'] = '';
    this.certDataMap.subject['UID'] = ''; // LDAP user Id or entity Id
    this.certDataMap.attributes['KEYPURPOSE'] = 'personal'; // This attribute is fixed for cert type
  }

  public emptyParamMapForSoftSigner(): void {
    this.certDataMap = {
      subject: {},
      attributes: {},
    };
    this.certDataMap.subject['CN'] = ''; // Canonical name
    this.certDataMap.subject['O'] = ''; // Organization
    this.certDataMap.subject['OU'] = ''; // Organizational unit
    this.certDataMap.subject['L'] = ''; // Locality name (e.g. city)
    this.certDataMap.subject['C'] = ''; // 2-char country code
    this.certDataMap.subject['E'] = '';
    this.certDataMap.subject['UID'] = ''; // LDAP user Id or entity Id
    this.certDataMap.attributes['KEYPURPOSE'] = 'softsign'; // This attribute is fixed for cert type
  }

  public static getSubjectFromCertificate(certificate: CertificationRequest): SubjectProperties {
    const subjectData: any = {};
    const supportedAttributes: any = this.getSupportedSubjectAttributes();
    Object.keys(supportedAttributes).map((key: string) => {
      const attribute = certificate.subject.typesAndValues.find((attribute: any) => {
        return supportedAttributes[key] === attribute.type;
      });
      if (attribute) {
        subjectData[key] = attribute.value.valueBlock.value;
      }
    });
    return subjectData;
  }

  private static getSupportedSubjectAttributes(): SubjectProperties {
    const subject: SubjectProperties = {};
    subject['CN'] = '2.5.4.3';
    subject['C'] = '2.5.4.6';
    subject['O'] = '2.5.4.10';
    subject['T'] = '2.5.4.12';
    subject['OU'] = '2.5.4.11';
    subject['L'] = '2.5.4.7';
    subject['ST'] = '2.5.4.8';
    subject['SERIALNUMBER'] = '2.5.4.5';
    subject['E'] = '1.2.840.113549.1.9.1';
    subject['DC'] = '0.9.2342.19200300.100.1.25';
    subject['UID'] = '0.9.2342.19200300.100.1.1';
    subject['STREET'] = '2.5.4.9';
    subject['SURNAME'] = '2.5.4.4';
    subject['GIVENNAME'] = '2.5.4.42';
    subject['INITIALS'] = '2.5.4.43';
    subject['GENERATION'] = '2.5.4.44';
    subject['unstructuredAddress'] = '1.2.840.113549.1.9.8';
    subject['unstructuredName'] = '1.2.840.113549.1.9.2';
    subject['UniqueIdentifier'] = '2.5.4.45';
    subject['DN'] = '2.5.4.46';
    subject['Pseudonym'] = '2.5.4.65';
    subject['PostalAddress'] = '2.5.4.16';
    subject['NameAtBirth'] = '1.3.36.8.3.14';
    subject['CountryOfCitizenship'] = '1.3.6.1.5.5.7.9.4';
    subject['CountryOfResidence'] = '1.3.6.1.5.5.7.9.5';
    subject['Gender'] = '1.3.6.1.5.5.7.9.3';
    subject['PlaceOfBirth'] = '1.3.6.1.5.5.7.9.2';
    subject['DateOfBirth'] = '1.3.6.1.5.5.7.9.1';
    subject['PostalCode'] = '2.5.4.17';
    subject['BusinessCategory'] = '2.5.4.15';
    subject['TelephoneNumber'] = '2.5.4.20';
    subject['Name'] = '2.5.4.41';
    subject['organizationIdentifier'] = '2.5.4.97';
    return subject;
  }
}
