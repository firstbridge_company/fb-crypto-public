/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import AEADCiphered from './dataformat/AEADCiphered';
import AEADPlain from './dataformat/AEADPlain';

/**
 * Encrypt/decrypt operations interface
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface Cryptor {
  /**
   * Encrypt message
   * @param plain plain text
   * @return encrypted text. Format depends on implementation and crypto scheme.
   */
  encrypt(plain: Uint8Array): Promise<Uint8Array>;

  /**
   * Decrypt message
   * @param ciphered encrypted text prefixed with 12 bytes of IV
   * @return plain text
   */
  decrypt(ciphered: Uint8Array): Promise<Uint8Array>;

  /**
   * Encrypt plain text, using shared key
   * unencrypted authenticated associated data
   * AES-GCM is used
   * @param plain plain text to encrypt
   * @param aeadata data to add unencrypted but authenticated by HMAC
   * @return specially formatted data that includes IV length in bytes
   * (4bytes), IV itself (variable part), unencrypted data length (4 bytes),
   * unencrypted data and then encrypted data in the rest of message;
   */
  encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<AEADCiphered>;

  /**
   * Decrypt AEADPlain message ciphered with key derived from asymmetric keys
   * AES-GCM is used
   * @param message specially formatted message, @see AEADCiphered
   * @return decrypted and verified data in AEADPlain structure
   */
  decryptWithAEAData(message: Uint8Array): Promise<AEADPlain>;
}
