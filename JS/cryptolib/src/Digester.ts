/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Interface to digesters
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface Digester {
  /**
   * Default digest algorithm defined by FBCryptoParams
   * @param message
   * @return
   */
  digest(message?: Uint8Array): Uint8Array;

  /**
   * Create and return MessageDigest for specified digester parameter
   * @return MessageDigest object for algorithm specified by FBCryptoParams
   */
  // digest(): MessageDigest;

  /**
   * Hash algorithms defined in FIPS PUB 180-4. SHA-256
   * @param message
   * @return
   */
  sha256(message: Uint8Array): Uint8Array;

  /**
   * Hash algorithms defined in FIPS PUB 180-4. SHA-512
   * @param message
   * @return]
   */
  sha512(message: Uint8Array): Uint8Array;

  /**
   * Permutation-based hash and extendable-output functions as defined in FIPS PUB 202.
   * SHA-3 256 bit
   * @param message
   * @return
   */
  sha3_256(message: Uint8Array): Uint8Array;

  /**
   * Permutation-based hash and extendable-output functions as defined in FIPS PUB 202.
   * SHA-3 384 bit
   * @param message
   * @return
   */
  sha3_384(message: Uint8Array): Uint8Array;

  /**
   * Permutation-based hash and extendable-output functions as defined in FIPS PUB 202.
   * SHA-3 512 bit
   * @param message
   * @return
   */
  sha3_512(message: Uint8Array): Uint8Array;

  PBKDF2(passPhrase: string, salt: Uint8Array): Uint8Array;
}
