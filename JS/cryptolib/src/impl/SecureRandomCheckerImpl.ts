/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import SecureRandomChecker from '../SecureRandomChecker';

/**
 * Check if SecureRandom is fast enough
 */
export default class SecureRandomCheckerImpl implements SecureRandomChecker {
  // SecureRandom srand = null;
  // public Long initDuration=0L;
  // public Long getDuration = 0L;
  public static GET_SIZE: number = 512;
  public static GET_ITERATIONS: number = 32;

  constructor() {
    // Long begin = System.currentTimeMillis();
    // srand = new SecureRandom();
    // Long end = System.currentTimeMillis();
    // initDuration = end - begin;
  }

  public check(): boolean {
    /*let rnd: number[] = new Array(SecureRandomCheckerImpl.GET_SIZE);
       Long begin = System.currentTimeMillis();
       for(let i = 0; i < SecureRandomCheckerImpl.GET_ITERATIONS; i++) {
           // srand.nextBytes(rnd);
       }
       Long end = System.currentTimeMillis();
       getDuration = end - begin;
        const res: boolean = (getDuration < SECURE_RANDOM_ACCEPTABLE_TIME_MS);
       return res;*/
    return true;
  }
}
