/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from '../CryptoParams';
import AsymCryptorDH from '../AsymCryptorDH';
import CryptoSignatureImpl from './CryptoSignatureImpl';
import CryptoSignature from '../CryptoSignature';
import AsymKeysHolder from '../AsymKeysHolder';
import AsymJCEECDHImpl from './ecc/AsymJCEECDHImpl';

/**
 * Common for ECC and RSA key generator methods
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class AbstractAsymDH implements AsymCryptorDH {
  private _privateKey!: CryptoKey;
  private _ourPublicKey!: CryptoKey;
  private _theirPublicKey!: CryptoKey;
  private _sharedKey!: CryptoKeyPair;
  private _ephemeralKeys!: CryptoKeyPair;
  private _params: CryptoParams;
  protected signer: CryptoSignature;

  constructor(params: CryptoParams) {
    this._params = params;
    this.signer = new CryptoSignatureImpl(this._params);
  }

  /**
   * Set all required keys
   * @param keys our public and private keys, their public key
   */
  public setKeys(keys: AsymKeysHolder): void {
    this._ourPublicKey = keys.ourPublicKey;
    this._privateKey = keys.privateKey;
    this._theirPublicKey = keys.theirPublicKey;
    this.signer.setKeys(keys);
    try {
      // blockCipherAsym = Cipher.getInstance(params.getAsymCipher());
      // AsymJCEECDHImpl.calculateSharedKey();
    } catch (ex) {
      console.error('Can not create cipher for ' + this._params.asymCipher);
    }
  }

  ecdheStep1(): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  ecdheStep2(signedEphemeralPubKey: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  calculateSharedKey(): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  encrypt(plain: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<import('../dataformat/AEADCiphered').default> {
    throw new Error('Method not implemented.');
  }
  decryptWithAEAData(message: Uint8Array): Promise<import('../dataformat/AEADPlain').default> {
    throw new Error('Method not implemented.');
  }

  get params(): CryptoParams {
    return this._params;
  }

  set params(value: CryptoParams) {
    this._params = value;
  }

  get privateKey(): CryptoKey {
    return this._privateKey;
  }

  set privateKey(value: CryptoKey) {
    this._privateKey = value;
  }

  get ourPublicKey(): CryptoKey {
    return this._ourPublicKey;
  }

  set ourPublicKey(value: CryptoKey) {
    this._ourPublicKey = value;
  }

  get theirPublicKey(): CryptoKey {
    return this._theirPublicKey;
  }

  set theirPublicKey(value: CryptoKey) {
    this._theirPublicKey = value;
  }

  get sharedKey(): CryptoKeyPair {
    return this._sharedKey;
  }

  set sharedKey(value: CryptoKeyPair) {
    this._sharedKey = value;
  }

  get ephemeralKeys(): CryptoKeyPair {
    return this._ephemeralKeys;
  }

  set ephemeralKeys(value: CryptoKeyPair) {
    this._ephemeralKeys = value;
  }
}
