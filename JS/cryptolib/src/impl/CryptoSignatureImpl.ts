/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from '../CryptoParams';
import CryptoSignature from '../CryptoSignature';
import AsymKeysHolder from '../AsymKeysHolder';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Common implementation of signature for all crypto systems supported by the library
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class CryptoSignatureImpl implements CryptoSignature {
  private _privateKey!: CryptoKey;
  private _ourPublicKey!: CryptoKey;
  private _theirPublicKey!: CryptoKey;

  private params: CryptoParams;

  constructor(params: CryptoParams) {
    this.params = params;
  }

  public setKeys(keys: AsymKeysHolder): void {
    this._ourPublicKey = keys.ourPublicKey;
    this._privateKey = keys.privateKey;
    this._theirPublicKey = keys.theirPublicKey;
  }

  public async sign(message: Uint8Array): Promise<Uint8Array> {
    try {
      let privateKey: CryptoKey = this._privateKey;
      if (privateKey.algorithm.name !== 'ECDSA') {
        const key: ArrayBuffer = await crypto.subtle.exportKey('pkcs8', privateKey);
        privateKey = await crypto.subtle.importKey('pkcs8', key, { name: 'ECDSA', namedCurve: 'P-521' }, true, [
          'deriveBits',
          'sign',
        ]);
      }
      const sign = await crypto.subtle.sign(
        { name: 'ECDSA', hash: { name: this.params.digester } },
        privateKey,
        message,
      );
      return new Uint8Array(sign);
    } catch (ex) {
      throw new Error(`Signing error.\n${ex.message}`);
    }
  }

  public async verify(message: Uint8Array, signature: Uint8Array): Promise<boolean> {
    try {
      let publicKey = this._theirPublicKey;
      if (publicKey.algorithm.name !== 'ECDSA') {
        const key: ArrayBuffer = await crypto.subtle.exportKey('spki', publicKey);
        publicKey = await crypto.subtle.importKey('spki', key, { name: 'ECDSA', namedCurve: 'P-521' }, true, [
          'verify',
        ]);
      }
      const result = await crypto.subtle.verify(
        { name: 'ECDSA', hash: { name: this.params.digester } },
        publicKey,
        signature.buffer,
        message.buffer,
      );
      return result;
    } catch (ex) {
      throw new Error(`Signature check exception.\n${ex}`);
    }
  }

  public add2BeginningOfArray(elements: Uint8Array, element: any): Uint8Array {
    return new Uint8Array();
  }

  private static toAsn1Primitive(data: Uint8Array): any {
    return;
  }

  public async signPlain(message: Uint8Array): Promise<Uint8Array> {
    return new Uint8Array();
  }

  public verifyPlain(message: Uint8Array, signature: Uint8Array): Promise<boolean> {
    return Promise.resolve(false);
  }

  get privateKey(): CryptoKey {
    return this._privateKey;
  }

  set privateKey(value: CryptoKey) {
    this._privateKey = value;
  }

  get ourPublicKey(): CryptoKey {
    return this._ourPublicKey;
  }

  set ourPublicKey(value: CryptoKey) {
    this._ourPublicKey = value;
  }

  get theirPublicKey(): CryptoKey {
    return this._theirPublicKey;
  }

  set theirPublicKey(value: CryptoKey) {
    this._theirPublicKey = value;
  }
}
