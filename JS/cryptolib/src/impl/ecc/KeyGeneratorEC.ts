/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import AbstractKeyGenerator from '../AbstractKeyGenerator';
import CryptoParams from '../../CryptoParams';
import CertificateRequestData from '../../csr/CertificateRequestData';
import NotRandom from '../NotRandom';

const ByteBuffer = require('bytebuffer');
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Generators for crypto keys and nounces
 */
export default class KeyGeneratorEC extends AbstractKeyGenerator {
  constructor(params: CryptoParams) {
    super(params);
  }

  /**
   * Generated true secure ECC or RSA key pair using secure random number generator
   *
   * @return key pair
   */
  public async generateKeys(secretPhrase?: string, salt?: Uint8Array): Promise<CryptoKeyPair> {
    if (!secretPhrase || !salt) {
      try {
        return await crypto.subtle.generateKey({ name: 'ECDSA', namedCurve: 'P-521' }, true, ['sign', 'verify']);
      } catch (ex) {
        throw new Error(ex.message);
      }
    } else {
      // const pair: KeyPair;
      const hashes = new Uint8Array(KeyGeneratorEC.NOT_RANDOM_LEN);
      const bb = ByteBuffer.wrap(hashes);
      while (bb.offset < KeyGeneratorEC.NOT_RANDOM_LEN) {
        const hash: Uint8Array = await this.deriveFromSecretPhrase(secretPhrase, salt, 256);
        const have: number = bb.offset;
        const empty: number = bb.capacity() - have;
        let toPut: number;
        if (empty >= hash.length) {
          toPut = hash.length;
        } else {
          toPut = empty - hash.length - 1;
        }
        bb.append(hash);
      }
      const srand = new NotRandom();
      srand.setSeed(bb.buffer);
      try {
        // TODO: generate key pair with prepared buffer
        const keyPair = await crypto.subtle.generateKey({ name: 'ECDSA', namedCurve: 'P-521' }, true, [
          'sign',
          'verify',
        ]);
        return keyPair;
      } catch (ex) {
        const msg = 'Invalid key generation parameters.';
        console.error(msg, ex);
        throw new Error(`${msg}\n${ex}`);
      }
      // return pair;
    }
  }

  /**
   * Generate ECDSA PublicKey X509 encoded
   *
   * @param bytes
   * @return
   */
  public createPublicKeyFromBytes(bytes: Uint8Array): any {
    throw new Error('Method not implemented.');
  }

  /**
   * Simple deterministic key derivation function. It is one-way function. It
   * calsulates hash (defined in params) of secretPhrase.getBytes() and salt.
   * If there is not enough bytes (keyLen) it uses hash result and the same
   * salt agian and puts additional bytes to output.
   *
   * @param secretPhrase UTF-8 encoded string
   * @param salt random salt at least of 16 bytes
   * @param keyLen desired output lenght
   * @return array of bytes that is determined by secretPhrase ans salt. It is
   * hard to calculate secretPthrase from it becuase it uses string
   * cryptographical hashing function SHA-512
   * @throws CryptoNotValidException
   */
  public deriveFromPasssPhrase(secretPhrase: string, salt: Uint8Array, keyLen: number): Uint8Array {
    throw new Error('Method not implemented.');
  }

  public createX509CertificateRequest(
    kp: any,
    certData: CertificateRequestData,
    allowCertSign: boolean,
    challengePassword: string,
  ): any {
    throw new Error('Method not implemented.');
  }

  public createSerlfSignedX509v3(kp: any, certData: CertificateRequestData): any {
    throw new Error('Method not implemented.');
  }
}
