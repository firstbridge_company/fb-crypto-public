import AbstractAsymCryptor from '../AbstractAsymCryptor';
import AsymCryptor from '../../AsymCryptor';
import CryptoParams from '../../CryptoParams';
import AEADCiphered from '../../dataformat/AEADCiphered';
import AEADPlain from '../../dataformat/AEADPlain';

export default class AsymJCEIESImpl extends AbstractAsymCryptor implements AsymCryptor {
  public constructor(params: CryptoParams) {
    super(params);
  }

  decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }

  encrypt(plain: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }

  encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<AEADCiphered> {
    throw new Error('AEAD operation are NON supported in IES mode');
  }

  decryptWithAEAData(message: Uint8Array): Promise<AEADPlain> {
    throw new Error('AEAD operation are NON supported in IES mode');
  }
}
