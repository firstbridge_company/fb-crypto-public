/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import * as _ from 'lodash';
import { stringToArrayBuffer } from 'pvutils';
import SymCryptor from '../../SymCryptor';
import CryptoParams from '../../CryptoParams';
import AEADCiphered from '../../dataformat/AEADCiphered';
import AEADPlain from '../../dataformat/AEADPlain';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 *
 */
export default class SymJCEImpl implements SymCryptor {
  symmetricKey?: CryptoKey;
  gcmIV: Uint8Array;
  params: CryptoParams;

  constructor(params: CryptoParams) {
    this.params = params;
    this.gcmIV = new Uint8Array(params.symIvLen);
  }

  /**
   * Set key for symmetric cipher
   *
   * @param key 128 or 256 bits key
   */
  public async setKey(key: Uint8Array): Promise<void> {
    if (!(key.byteLength === 128 / 8 || key.byteLength === 256 / 8)) {
      throw new Error('Key length must be exactly 16 or 32 or bytes long');
    }
    // try {
    this.symmetricKey = await crypto.subtle.importKey('raw', key, 'AES-GCM', true, ['encrypt', 'decrypt']);
    //     symmetrciKey = new SecretKeySpec(key, "AES");
    //     blockCipherSym = Cipher.getInstance(params.getSymCipher());
    // } catch (ex: NoSuchAlgorithmException | NoSuchPaddingException) {
    //     console.log.warn(ex.getMessage());
    //     throw new CryptoNotValidException(ex.getMessage(), ex);
    // }
  }

  public setIV(iv: Uint8Array): void {
    this.gcmIV.set(iv, 0);
  }

  public getIV(): Uint8Array {
    return this.gcmIV;
  }

  public setSalt(salt: Uint8Array): void {
    this.gcmIV.set(salt, 0);
  }

  public getSalt(): Uint8Array {
    return Object.assign(new Uint8Array(this.params.symGcmSaltLen), this.gcmIV);
  }

  public setNonce(explicitNonce: Uint8Array | null): void {
    if (_.isEqual(this.getNonce(), explicitNonce)) {
      throw new Error('Nonce reuse detected!');
    }
    let en: Uint8Array;
    if (explicitNonce === null) {
      en = new Uint8Array(this.params.symGcmNonceLen);
      crypto.getRandomValues(en);
    } else {
      en = _.clone(explicitNonce);
    }
    this.gcmIV.set(en, this.params.symGcmSaltLen);
  }

  public getNonce(): Uint8Array {
    return this.gcmIV.slice(this.params.symGcmSaltLen);
  }

  public async encrypt(plain: Uint8Array): Promise<Uint8Array> {
    try {
      let encrypted = new ArrayBuffer(0);
      if (this.symmetricKey) {
        encrypted = await crypto.subtle.encrypt(
          {
            name: 'AES-GCM',
            iv: this.gcmIV,
            tagLength: this.params.gcmAuthTagLenBits,
          },
          this.symmetricKey,
          plain,
        );
      }
      const bb: Uint8Array = new Uint8Array(encrypted.byteLength + this.params.symGcmNonceLen);
      bb.set(this.getNonce());
      bb.set(new Uint8Array(encrypted), this.params.symGcmNonceLen);
      return bb;
    } catch (e) {
      throw new Error(`Invalid symmetric key.\n${e}`);
    }
  }

  public async decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    const explicitNonceSym = ciphered.slice(0, this.params.symGcmNonceLen);
    this.setNonce(explicitNonceSym);
    const cipherText = ciphered.slice(this.params.symGcmNonceLen);

    let decrypted = new Uint8Array();
    try {
      if (this.symmetricKey) {
        const dec = await crypto.subtle.decrypt(
          {
            name: 'AES-GCM',
            iv: this.gcmIV,
            tagLength: this.params.gcmAuthTagLenBits,
          },
          this.symmetricKey,
          cipherText,
        );
        decrypted = new Uint8Array(dec);
      }
      return decrypted;
    } catch (e) {
      throw new Error(e);
    }
  }

  public async encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array | string): Promise<AEADCiphered> {
    if (typeof aeadata === 'string') {
      aeadata = new Uint8Array(stringToArrayBuffer(aeadata));
    }
    try {
      const msg: AEADCiphered = new AEADCiphered(this.params);
      if (this.symmetricKey) {
        const encrypted = await crypto.subtle.encrypt(
          {
            name: 'AES-GCM',
            iv: this.gcmIV,
            tagLength: this.params.gcmAuthTagLenBits,
            additionalData: aeadata,
          },
          this.symmetricKey,
          plain,
        );
        msg.encrypted = new Uint8Array(encrypted);
      }
      if (aeadata != null) {
        msg.aatext = aeadata;
      }
      msg.setExplicitNonce(this.getNonce());
      return msg;
    } catch (ex) {
      throw new Error(`Invalid symmetric key\n${ex}`);
    }
  }

  public async decryptWithAEAData(message: Uint8Array): Promise<AEADPlain> {
    try {
      const res: AEADPlain = new AEADPlain();
      const msg: AEADCiphered = AEADCiphered.fromBytes(message, this.params);
      this.setNonce(msg.getExplicitNonce());
      if (this.symmetricKey) {
        const decrypted = await crypto.subtle.decrypt(
          {
            name: 'AES-GCM',
            iv: this.gcmIV,
            tagLength: this.params.gcmAuthTagLenBits,
            additionalData: msg.aatext,
          },
          this.symmetricKey,
          msg.encrypted,
        );
        res.decrypted = new Uint8Array(decrypted);
      }
      res.plain = msg.aatext;
      res.hmacOk = true;
      return res;
    } catch (e) {
      throw new Error(e);
    }
  }
}
