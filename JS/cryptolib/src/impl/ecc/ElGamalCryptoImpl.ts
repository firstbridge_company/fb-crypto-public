/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import ElGamalCrypto from '../../ElGamalCrypto';
import CryptoParams from '../../CryptoParams';

/**
 *
 */
export default class ElGamalCryptoImpl implements ElGamalCrypto {
  params: CryptoParams;

  constructor(params: CryptoParams) {
    this.params = params;
  }

  decrypt(priKey: CryptoKey, cryptogram: any): any {
    throw new Error('Method not implemented.');
  }

  encrypt(publicKeyX: any, publicKeyY: any, plainText: any): any {
    throw new Error('Method not implemented.');
  }

  generateOwnKeys(): any {
    throw new Error('Method not implemented.');
  }

  getPrivateKey(): any {
    throw new Error('Method not implemented.');
  }

  getPublicKeyX(): any {
    throw new Error('Method not implemented.');
  }

  getPublicKeyY(): any {
    throw new Error('Method not implemented.');
  }
}
