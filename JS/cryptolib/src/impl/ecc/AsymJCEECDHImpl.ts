/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import ByteBuffer from '../../dataformat/ByteBuffer';
import AbstractAsymDH from '../AbstractAsymDH';
import CryptoParams from '../../CryptoParams';
import AsymCryptorDH from '../../AsymCryptorDH';
import KeyGeneratorEC from './KeyGeneratorEC';
import AEADCiphered from '../../dataformat/AEADCiphered';
import AEADPlain from '../../dataformat/AEADPlain';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();
const BYTES = 4;

export default class AsymJCEECDHImpl extends AbstractAsymDH implements AsymCryptorDH {
  public constructor(params: CryptoParams) {
    super(params);
  }

  public async ecdheStep1(): Promise<Uint8Array> {
    const kg: KeyGeneratorEC = new KeyGeneratorEC(this.params);
    this.ephemeralKeys = await kg.generateKeys();
    const key: ArrayBuffer = await crypto.subtle.exportKey('spki', this.ephemeralKeys.publicKey);
    const signature: Uint8Array = await this.signer.sign(new Uint8Array(key));
    const cap: number = BYTES + key.byteLength + signature.byteLength;
    const bb: Uint8Array = ByteBuffer.allocate(cap);
    bb.set(new Uint8Array(key), BYTES);
    bb.set(signature, BYTES + key.byteLength);
    const view = new DataView(bb.buffer);
    view.setUint32(0, key.byteLength);
    return new Uint8Array(view.buffer);
  }

  public async ecdheStep2(signedEphemeralPubKey: Uint8Array): Promise<Uint8Array> {
    const bb = new ByteBuffer(signedEphemeralPubKey);
    const keySize: number = bb.getInt();
    const key = bb.get(keySize);
    const signature = bb.get(signedEphemeralPubKey.byteLength - keySize - BYTES);
    const ok: boolean = await this.signer.verify(key, signature);
    if (!ok) {
      throw new Error('ECDHE public key signature is not valid!');
    }

    try {
      const theirPub = await crypto.subtle.importKey('spki', key, { name: 'ECDH', namedCurve: 'P-521' }, true, [
        'deriveBits',
        'verify',
      ]);
      let privateKey: CryptoKey = this.ephemeralKeys.privateKey;
      if (privateKey.algorithm.name === 'ECDSA') {
        const keyPkcs: ArrayBuffer = await crypto.subtle.exportKey('pkcs8', privateKey);
        privateKey = await crypto.subtle.importKey('pkcs8', keyPkcs, { name: 'ECDH', namedCurve: 'P-521' }, true, [
          'deriveBits',
          'sign',
        ]);
      }
      let publicKey: CryptoKey = this.ephemeralKeys.publicKey;
      if (publicKey.algorithm.name === 'ECDSA') {
        const keyJwk: JsonWebKey = await crypto.subtle.exportKey('jwk', publicKey);
        publicKey = await crypto.subtle.importKey('jwk', keyJwk, { name: 'ECDH', namedCurve: 'P-521' }, true, [
          'deriveBits',
          'verify',
        ]);
      }
      const skh = await this.doCalculateShared(publicKey, privateKey, theirPub);
      return skh;
    } catch (ex) {
      throw new Error(ex);
    }
  }

  public async doCalculateShared(ourPub: CryptoKey, ourPriv: CryptoKey, theirPub: CryptoKey): Promise<Uint8Array> {
    const algorithm = { name: 'ECDH', namedCurve: 'P-521', public: theirPub };
    const sk = await crypto.subtle.deriveBits(algorithm, ourPriv, 528);
    const secretKeyExported = new Uint8Array(sk);
    const ourPublicKeyExported = await crypto.subtle.exportKey('spki', ourPub);
    const theirPublicKeyExported = await crypto.subtle.exportKey('spki', theirPub);
    const keys = [new Uint8Array(ourPublicKeyExported), new Uint8Array(theirPublicKeyExported)];
    keys.sort();
    const data = new Int8Array(secretKeyExported.byteLength + keys[0].byteLength + keys[1].byteLength);
    data.set(secretKeyExported);
    data.set(keys[0], secretKeyExported.byteLength);
    data.set(keys[1], secretKeyExported.byteLength + keys[0].byteLength);
    const skh = await crypto.subtle.digest(this.params.keyAgreementDigester, data);
    return new Uint8Array(skh);
  }

  public async calculateSharedKey(): Promise<Uint8Array> {
    try {
      const skh: Uint8Array = await this.doCalculateShared(this.ourPublicKey, this.privateKey, this.theirPublicKey);
      this.sharedKey = await crypto.subtle.importKey('raw', skh, 'AES-GCM', true, ['encrypt', 'decrypt']);
      return skh;
    } catch (ex) {
      throw new Error(ex);
    }
  }

  async encrypt(plain: Uint8Array): Promise<Uint8Array> {
    try {
      const iv = new Uint8Array(this.params.symIvLen);
      crypto.getRandomValues(iv);

      const encrypted: ArrayBuffer = await crypto.subtle.encrypt(
        {
          name: 'AES-GCM',
          iv,
        },
        this.sharedKey,
        plain,
      );
      const bb: Uint8Array = new Uint8Array(encrypted.byteLength + iv.byteLength);
      bb.set(iv, 0);
      bb.set(new Uint8Array(encrypted), iv.byteLength);
      return bb;
    } catch (ex) {
      throw new Error(`Invalid asymmetric key.\n${ex.message}`);
    }
  }

  async decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    try {
      const cipherText = ciphered.slice(this.params.symIvLen);
      const iv = ciphered.slice(0, this.params.symIvLen);

      const decrypted = await crypto.subtle.decrypt(
        {
          name: 'AES-GCM',
          iv,
          tagLength: this.params.gcmAuthTagLenBits,
        },
        this.sharedKey,
        cipherText,
      );
      return new Uint8Array(decrypted);
    } catch (ex) {
      throw new Error(`Invalid asymmetric key.\n${ex.message}`);
    }
  }

  async encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<any> {
    try {
      const iv = new Uint8Array(this.params.symIvLen);
      crypto.getRandomValues(iv);
      const msg: AEADCiphered = new AEADCiphered(this.params);
      const encrypted = await crypto.subtle.encrypt(
        {
          name: 'AES-GCM',
          iv,
          tagLength: this.params.gcmAuthTagLenBits,
          additionalData: aeadata,
        },
        this.sharedKey,
        plain,
      );
      msg.encrypted = new Uint8Array(encrypted);
      if (aeadata !== null) {
        msg.aatext = aeadata;
      }
      msg.iv = iv;
      return msg;
    } catch (ex) {
      throw new Error(`Invalid asymmetric key.\n${ex.message}`);
    }
  }

  async decryptWithAEAData(message: Uint8Array): Promise<any> {
    const res: AEADPlain = new AEADPlain();
    const msg: AEADCiphered = AEADCiphered.fromBytes(message, this.params);
    try {
      res.decrypted = await crypto.subtle.decrypt(
        {
          name: 'AES-GCM',
          iv: msg.iv,
          tagLength: this.params.gcmAuthTagLenBits,
          additionalData: msg.aatext,
        },
        this.sharedKey,
        msg.encrypted,
      );
      res.plain = msg.aatext;
      res.hmacOk = true;
      return res;
    } catch (ex) {
      throw new Error(`Invalid asymmetric key.\n${ex.message}`);
    }
  }
}
