/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import KeyWriter from '../KeyWriter';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Write different keys in standard formats
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class KeyWriterImpl implements KeyWriter {
  public writePvtKeyPEM(path: string, key: any): void {
    // try (PemWriter writer = new PemWriter(new FileWriter(path))) {
    //     writer.writeObject(new PemObject("PRIVATE KEY", key.getEncoded()));
    //     //TODO: encrypted? "ENCRYPTED PRIVATE KEY"
    // }
  }

  public writePvtKeyPKCS12(path: string): void {
    // throw new UnsupportedOperationException("Not supported yet.");
  }

  public writeCertificateRequestPEM(path: string, cr: any): void {
    // try (PemWriter writer = new PemWriter(new FileWriter(path))) {
    //     byte[] enc = cr.getEncoded();
    //     writer.writeObject(new PemObject("CERTIFICATE REQUEST", enc));
    // }
  }

  public writeX509CertificatePEM(path: string, certificate: any): void {
    if (certificate == null) {
      // throw new IllegalArgumentException("certificate must be defined.");
    }
    // try (PemWriter writer = new PemWriter(new FileWriter(path))) {
    //         writer.writeObject(new PemObject("CERTIFICATE", certificate.getEncoded()));
    // } catch (CertificateEncodingException e) {
    //     throw new RuntimeException("Problem wirh a certificate", e);
    // }
  }

  public addX509CertToPKCS12(certificate: any, pathToJKS: string, alias: string, jksPassword: string): void {
    // throw new UnsupportedOperationException("Not supported yet.");
  }

  public async serializePublicKey(pubk: any): Promise<Uint8Array> {
    const key = await crypto.subtle.exportKey('raw', pubk);
    return new Uint8Array(key);
  }

  public async serializePrivateKey(privk: any): Promise<Uint8Array> {
    const key = await crypto.subtle.exportKey('raw', privk);
    return new Uint8Array(key);
  }

  public getCertificateRequestPEM(cr: any): string {
    // StringWriter sw = new StringWriter(2048);
    // PemWriter writer = new PemWriter(sw);
    // byte[] enc = cr.getEncoded();
    // writer.writeObject(new PemObject("CERTIFICATE REQUEST", enc));
    // writer.flush();
    // return sw.toString();
    return '';
  }

  public getPvtKeyPEM(key: any): string {
    // StringWriter sw = new StringWriter(2048);
    // PemWriter writer = new PemWriter(sw);
    // writer.writeObject(new PemObject("PRIVATE KEY", key.getEncoded()));
    // writer.flush();
    // return sw.toString();
    return '';
  }

  public getX509CertificatePEM(certificate: any): string {
    if (certificate == null) {
      // throw new IllegalArgumentException("certificate must be defined.");
    }
    // StringWriter sw = new StringWriter();
    // try (PemWriter writer = new PemWriter(sw)) {
    //     writer.writeObject(new PemObject("CERTIFICATE", certificate.getEncoded()));
    //     writer.flush();
    // } catch (CertificateEncodingException e) {
    //     throw new RuntimeException("Problem wirh a certificate", e);
    // }
    // return sw.toString();
    return '';
  }
}
