/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

// @ts-ignore
import { ECPublicKey, RSAPublicKey } from 'pkijs';
import * as asn1js from 'asn1js';
import { verify } from 'crypto';
const pkijs = require('pkijs');
import KeyReader from '../KeyReader';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Reads keys in various formats and returns in the form acceptable by FBCrypto
 *
 *  @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class KeyReaderImpl implements KeyReader {
  public async extractPublicKeyFromX509(certificate: any): Promise<CryptoKey> {
    try {
      const pubKey: RSAPublicKey | ECPublicKey = certificate.subjectPublicKeyInfo.parsedKey;
      const jwkKey = pubKey.toJSON();
      if (pubKey instanceof RSAPublicKey) {
        jwkKey.kty = 'RSA';
        jwkKey.alg = 'RSA-OAEP-256';
        // const keyData = new Uint8Array(pubKey.modulus.valueBlock.valueHex);
        // const importedKey = await crypto.subtle.importKey("spki", keyData, { name: "RSA-OAEP", hash: "SHA-512" }, true, [
        //   'encrypt',
        //   'decrypt',
        //   ]);
        const importedKey = await crypto.subtle.importKey(
          'jwk',
          jwkKey,
          { name: 'RSA-OAEP', hash: { name: 'SHA-256' } },
          true,
          ['encrypt', 'decrypt'],
        );
        return importedKey;
      } else if (pubKey instanceof ECPublicKey) {
        jwkKey.kty = 'EC';
        return await crypto.subtle.importKey('jwk', jwkKey, { name: 'ECDH', namedCurve: 'P-521' }, true, [
          'deriveBits',
          'verify',
        ]);
      } else {
        throw new Error('Unknown encryption in certificate, please use ECC');
      }
    } catch (ex) {
      throw new Error(ex);
    }
  }

  public async readPrivateKeyPEM(input: string): Promise<CryptoKey> {
    try {
      const privateKeyData = this.convertPemToBinary(input);
      return await crypto.subtle.importKey(
        'pkcs8',
        privateKeyData.buffer,
        { name: 'ECDH', namedCurve: 'P-521' },
        true,
        ['deriveBits', 'sign'],
      );
    } catch (ex) {
      throw new Error(ex);
    }
  }

  public readEncryptedPrivateKeyPEM(input: any, password: string): any {
    // Reader rdr = new InputStreamReader(input);
    // Object parsed =  new PEMParser(rdr).readObject();
    // PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(password.toCharArray());
    // JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
    // PrivateKey key = converter.getPrivateKey((PrivateKeyInfo) parsed);
    //
    // return key;
  }

  public readPrivateKeyPKCS8(input: any): any {
    // try {
    //     byte[] buffer = new byte[16384];
    //     int size = input.read(buffer);
    //     byte[] bytes = Arrays.copyOf(buffer, size);
    //     /* Check to see if this is in an EncryptedPrivateKeyInfo structure. */
    //     PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(bytes);
    //     /*
    //      * Now it's in a PKCS#8 PrivateKeyInfo structure. Read its Algorithm
    //      * OID and use that to construct a KeyFactory.
    //      */
    //     ASN1InputStream bIn = new ASN1InputStream(new ByteArrayInputStream(spec.getEncoded()));
    //     PrivateKeyInfo pki = PrivateKeyInfo.getInstance(bIn.readObject());
    //     String algOid = pki.getPrivateKeyAlgorithm().getAlgorithm().getId();
    //     PrivateKey pk = KeyFactory.getInstance(algOid).generatePrivate(spec);
    //     return pk;
    // } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
    //     log.error("Can not read PKCS#8 private key: "+ex);
    // } finally {
    //     try {
    //         input.close();
    //     } catch (IOException ex) {
    //     }
    // }
    // return null;
  }

  public readPrivateKeyPKCS12(PKCS12filePath: string, password: string, keyPassword: string, alias: string): any {
    // KeyPair kp = readPKCS12File(PKCS12filePath, password, alias);
    // return kp.getPrivate();
  }

  public readPublicKeyPKCS12(PKCS12filePath: string, password: string, alias: string): any {
    // KeyPair kp = readPKCS12File(PKCS12filePath, password, alias);
    // return kp.getPublic();
  }

  public readPKCS12File(path: string, password: string, alias: string): any {
    // KeyStore ks = KeyStore.getInstance("PKCS12",FBCryptoParams.getProvider());
    // FileInputStream fis = new FileInputStream(path);
    // ks.load(fis, password.toCharArray());
    //
    // Enumeration aliasEnum = ks.aliases();
    //
    // Key key = null;
    // Certificate cert = null;
    // KeyPair kp = null;
    // while (aliasEnum.hasMoreElements()) {
    //     String keyName = (String) aliasEnum.nextElement();
    //     if (keyName.compareToIgnoreCase(alias) == 0) {
    //         key = ks.getKey(keyName, password.toCharArray());
    //         cert = ks.getCertificate(keyName);
    //         kp = new KeyPair(cert.getPublicKey(), (PrivateKey) key);
    //     }
    //
    // }
    //
    // return kp;
  }

  public static getCertFromPKCS12File(path: string, password: string, alias: string): any {
    // X509Certificate c = null;
    // KeyStore p12 = KeyStore.getInstance("PKCS12",FBCryptoParams.getProvider());
    // p12.load(new FileInputStream(path), password.toCharArray());
    // Enumeration e = p12.aliases();
    // while (e.hasMoreElements()) {
    //     String calias = (String) e.nextElement();
    //     if (alias.compareTo(calias) == 0) {
    //         c = (X509Certificate) p12.getCertificate(alias);
    //         Principal subject = c.getSubjectDN();
    //         String subjectArray[] = subject.toString().split(",");
    //         for (String s : subjectArray) {
    //             String[] str = s.trim().split("=");
    //             String key = str[0];
    //             String value = str[1];
    //             System.out.println(key + " - " + value);
    //         }
    //         break;
    //     }
    // }
    // return c;
  }

  public convertPemToBinary(pem: string): Uint8Array {
    pem = pem.replace(/([\n\r])/g, '');
    const b64 = /-----BEGIN [^-]+-----(.*?)-----END [^-]+-----/g.exec(pem) || [];
    const der = Buffer.from(b64[1], 'base64');
    return new Uint8Array(der);
  }

  public readX509CertPEMorDER(is: string): any {
    try {
      const ber = this.convertPemToBinary(is).buffer;
      const asn1 = asn1js.fromBER(ber);
      return new pkijs.Certificate({ schema: asn1.result });
    } catch (ex) {
      throw new Error(`Can not read X.509 certificate.\n${ex.message}`);
    }
  }

  /**
   * Reads public key from most standard and compatible representation (ASN.1 in X509)
   * Compatible with OpenSSL.
   * @param keyBytes bytes of public key
   * @return re-constructed public key
   * @throws io.firstbridge.cryptolib.CryptoNotValidException
   */
  public deserializePublicKey(keyBytes: Uint8Array): any {
    // try {
    //     X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
    //     KeyFactory kf = KeyFactory.getInstance("ECDSA", "BC");
    //     return kf.generatePublic(spec);
    // } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException ex) {
    //     throw new CryptoNotValidException("Unsupported or invalid ECC public key", ex);
    // }
  }

  /**
   * Reads private key from most standard and compatible representation (ASN.1 in PKSC#8)
   * Compatible with OpenSSL.
   * @param keyBytes bytes of private key
   * @return re-constructed private key
   */
  public deserializePrivateKey(keyBytes: Uint8Array): any {
    // try {
    //     const spec: PKCS8EncodedKeySpec =  new PKCS8EncodedKeySpec(keyBytes);
    //     KeyFactory kf = KeyFactory.getInstance("ECDSA", "BC");
    //     return kf.generatePrivate(spec);
    // } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException ex) {
    //     throw new CryptoNotValidException("Unsupported or invalid ECC public key", ex);
    // }
  }
}
