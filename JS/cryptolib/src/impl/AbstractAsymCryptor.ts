/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from '../CryptoParams';
import AsymKeysHolder from '../AsymKeysHolder';
import AsymCryptor from '../AsymCryptor';
import { Cipher, createCipheriv } from 'crypto';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Common part of asymmetric encryptor with integrated encryption schema implementations
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class AbstractAsymCryptor implements AsymCryptor {
  private _privateKey!: CryptoKey;
  private _ourPublicKey!: CryptoKey;
  private _theirPublicKey!: CryptoKey;
  private _params: CryptoParams;

  constructor(params: CryptoParams) {
    this._params = params;
  }

  /**
   * Set all required keys
   * @param keys our public and private keys, their public key
   */
  public setKeys(keys: AsymKeysHolder): void {
    this._ourPublicKey = keys.ourPublicKey;
    this._privateKey = keys.privateKey;
    this._theirPublicKey = keys.theirPublicKey;
  }

  encrypt(plain: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    throw new Error('Method not implemented.');
  }
  encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<import('../dataformat/AEADCiphered').default> {
    throw new Error('Method not implemented.');
  }
  decryptWithAEAData(message: Uint8Array): Promise<import('../dataformat/AEADPlain').default> {
    throw new Error('Method not implemented.');
  }

  get params(): CryptoParams {
    return this._params;
  }

  set params(value: CryptoParams) {
    this._params = value;
  }

  get privateKey(): CryptoKey {
    return this._privateKey;
  }

  set privateKey(value: CryptoKey) {
    this._privateKey = value;
  }

  get ourPublicKey(): CryptoKey {
    return this._ourPublicKey;
  }

  set ourPublicKey(value: CryptoKey) {
    this._ourPublicKey = value;
  }

  get theirPublicKey(): CryptoKey {
    return this._theirPublicKey;
  }

  set theirPublicKey(value: CryptoKey) {
    this._theirPublicKey = value;
  }
}
