/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import Digester from '../Digester';
import CryptoParams from '../CryptoParams';

/**
 *
 */
export default class JCEDigestImpl implements Digester {
  params: CryptoParams;

  public constructor(params: CryptoParams) {
    this.params = params;
  }

  public digest(message: Uint8Array): Uint8Array {
    // try {
    // MessageDigest hash = MessageDigest.getInstance(this.params.digester);
    // hash.update(message);
    // return hash.digest();
    // } catch (ex: NoSuchAlgorithmException) {
    //     throw new CryptoNotValidException("No "+this.params.digester+" defined",ex);
    // }
    return new Uint8Array();
  }

  // @Override
  // public MessageDigest digest()  throws CryptoNotValidException{
  //     try {
  //         return MessageDigest.getInstance(params.getDigester());
  //     } catch (NoSuchAlgorithmException ex) {
  //         throw new CryptoNotValidException("No "+params.getDigester()+" defined",ex);
  //     }
  // }

  public sha256(message: Uint8Array): Uint8Array {
    // try {
    //     MessageDigest hash = MessageDigest.getInstance("SHA-256");
    //     hash.update(message);
    //     return hash.digest();
    // } catch (NoSuchAlgorithmException ex) {
    //     throw new CryptoNotValidException("No SHA-256 defined",ex);
    // }
    return new Uint8Array();
  }

  public sha512(message: Uint8Array): Uint8Array {
    // try {
    //     MessageDigest hash = MessageDigest.getInstance("SHA-512");
    //     hash.update(message);
    //     return hash.digest();
    // } catch (NoSuchAlgorithmException ex) {
    //     throw new CryptoNotValidException("No SHA-512 defined",ex);
    // }
    return new Uint8Array();
  }

  public sha3_256(message: Uint8Array): Uint8Array {
    // try {
    //     MessageDigest hash = MessageDigest.getInstance("SHA3-256");
    //     hash.update(message);
    //     return hash.digest();
    // } catch (NoSuchAlgorithmException ex) {
    //     throw new CryptoNotValidException("No SH3A-256 defined",ex);
    // }
    return new Uint8Array();
  }

  public sha3_384(message: Uint8Array): Uint8Array {
    // try {
    //     MessageDigest hash = MessageDigest.getInstance("SHA3-384");
    //     hash.update(message);
    //     return hash.digest();
    // } catch (NoSuchAlgorithmException ex) {
    //     throw new CryptoNotValidException("No SH3A-384 defined",ex);
    // }
    return new Uint8Array();
  }

  public sha3_512(message: Uint8Array): Uint8Array {
    // try {
    //     MessageDigest hash = MessageDigest.getInstance("SHA3-512");
    //     hash.update(message);
    //     return hash.digest();
    // } catch (NoSuchAlgorithmException ex) {
    //     throw new CryptoNotValidException("No SH3A-512 defined",ex);
    // }
    return new Uint8Array();
  }

  public PBKDF2(passPhrase: string, salt: Uint8Array): Uint8Array {
    // if(salt==null){
    //     throw new CryptoNotValidException("Salt can not be null, lenght is 12 bytes for PBKDF2");
    // }
    // try {
    //     SecretKeyFactory skf = SecretKeyFactory.getInstance(FBCryptoParams.KEY_DERIVATION_FN);
    //     PBEKeySpec spec = new PBEKeySpec( passPhrase.toCharArray(), salt, FBCryptoParams.PBKDF2_ITERATIONS, FBCryptoParams.PBKDF2_KEYELEN);
    //     SecretKey key = skf.generateSecret(spec);
    //
    //     byte[] res = key.getEncoded( );
    //     return res;
    // } catch (NoSuchAlgorithmException ex) {
    //     //ignore, we use constants
    // } catch (InvalidKeySpecException ex) {
    //     throw new CryptoNotValidException("Possibly invaliud salt lenght for PBKDF2");
    // }
    // return null;
    return new Uint8Array();
  }
}
