/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();
import CryptoParams from '../CryptoParams';
import KeyGenerator from '../KeyGenerator';
import { stringToArrayBuffer } from 'pvutils';

/**
 * Common for ECC and RSA key generator methods
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default abstract class AbstractKeyGenerator implements KeyGenerator {
  public static NOT_RANDOM_LEN = 4096;
  private params: CryptoParams;

  constructor(params: CryptoParams) {
    this.params = params;
  }

  /**
   * Simple deterministic key derivation function. It is one-way function. It
   * calculates hash (defined in params) of secretPhrase.getBytes() and salt.
   * If there is not enough bytes (keyLen) it uses hash result and the same
   * salt agian and puts additional bytes to output.
   *
   * @param secretPhrase UTF-8 encoded string
   * @param salt random salt at least of 16 bytes
   * @param keyLenBits desired output length in bits (should be byte-aligned)
   * @return array of bytes that is determined by secretPhrase ans salt. It is
   * hard to calculate secretPhrase from it because it uses string
   * cryptographic hashing function SHA-512
   */
  async deriveFromSecretPhrase(secretPhrase: string, salt: Uint8Array, keyLenBits: number): Promise<Uint8Array> {
    let digester = this.params.digester;
    if (keyLenBits > 256) {
      digester = 'SHA-512'; //longes one
    }
    if (keyLenBits > 512) {
      throw new Error('Can not generate key longer then 512 bits');
    }
    const keyLen: number = keyLenBits / 8;
    const iterations: number = this.params.pbkdf2Iterations; //at the moment we do not have Pbkdf in JS
    const bb = new Uint8Array(keyLen);
    let input: ArrayBuffer;

    try {
      const secretPhraseBuffer = stringToArrayBuffer(secretPhrase);
      const inputBuffer = new Uint8Array(secretPhraseBuffer.byteLength + salt.byteLength);
      inputBuffer.set(new Uint8Array(secretPhraseBuffer));
      inputBuffer.set(salt, secretPhraseBuffer.byteLength);
      input = await crypto.subtle.digest(digester, inputBuffer);

      for (let i = 1; i < iterations; i++) {
        input = await crypto.subtle.digest(digester, input);
      }
    } catch (ex) {
      throw new Error(`Digest algorithm is not available.\n${ex}`);
    }

    const cutInput = new Uint8Array(input, 0, keyLen);
    bb.set(cutInput, 0);
    return bb;
  }

  generateSymKey(): Uint8Array {
    return new Uint8Array();
  }

  generateIV(): Uint8Array {
    return new Uint8Array();
  }

  generateKeys(secretPhrase?: string, salt?: Uint8Array): Promise<CryptoKeyPair> {
    throw new Error('Method not implemented.');
  }
}
