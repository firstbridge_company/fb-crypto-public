/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from '../../CryptoParams';
import AbstractAsymCryptor from '../AbstractAsymCryptor';
import AEADCiphered from '../../dataformat/AEADCiphered';
import AEADPlain from '../../dataformat/AEADPlain';
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * RSA based implementation of FBCrypto interface
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class AsymCryptorRSAImpl extends AbstractAsymCryptor {
  constructor(params: CryptoParams) {
    super(params);
  }

  /**
   * Default RSA encryption, weak, no IV or other data in output
   * @param plain plain text
   * @return encrypted text
   */
  public async encrypt(plain: Uint8Array): Promise<Uint8Array> {
    try {
      const encrypted: ArrayBuffer = await crypto.subtle.encrypt(
        {
          name: 'RSA-OAEP',
        },
        this.theirPublicKey,
        plain,
      );
      return new Uint8Array(encrypted);
    } catch (e) {
      throw new Error(`Encryption filed\n${e}`);
    }
  }

  /**
   * Default RSA decryption, weak no IV or other data in output
   * @param ciphered encrypted text prefixed with 12 bytes of IV
   * @return decrypted plain text
   */
  public async decrypt(ciphered: Uint8Array): Promise<Uint8Array> {
    try {
      const decrypted: ArrayBuffer = await crypto.subtle.decrypt(
        {
          name: 'RSA-OAEP',
        },
        this.privateKey,
        ciphered,
      );
      return new Uint8Array(decrypted);
    } catch (e) {
      throw new Error(`Decryption filed\n${e}`);
    }
  }

  public encryptWithAEAData(plain: Uint8Array, aeadata: Uint8Array): Promise<AEADCiphered> {
    throw new Error('AEAD is not supported in RSA mode.');
  }

  public decryptWithAEAData(message: Uint8Array): Promise<AEADPlain> {
    throw new Error('AEAD is not supported in RSA mode.');
  }
}
