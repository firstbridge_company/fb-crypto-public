/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import AbstractKeyGenerator from '../AbstractKeyGenerator';
import CryptoParams from '../../CryptoParams';
import CertificateRequestData from '../../csr/CertificateRequestData';

let crypto: any;
if (typeof window === 'undefined' || typeof window.crypto === 'undefined') {
  const { Crypto } = require('node-webcrypto-ossl');
  crypto = new Crypto();
} else {
  crypto = window.crypto;
}

/**
 * Generators for crypto keys and nonce
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class KeyGeneratorRSA extends AbstractKeyGenerator {
  public constructor(params: CryptoParams) {
    super(params);
  }

  /**
   * Generated true secure ECC or RSA key pair using secure random number generator
   *
   * @return key pair
   */
  public async generateKeys(): Promise<CryptoKeyPair> {
    try {
      return await crypto.subtle.generateKey(
        {
          name: 'RSA-OAEP',
          modulusLength: 4096,
          // publicExponent: new Uint8Array([1, 0, 1]),
          hash: 'SHA-256',
        },
        true,
        ['encrypt', 'decrypt'],
      );
    } catch (ex) {
      throw new Error(ex.message);
    }
  }

  /**
   * Generate ECDSA PublicKey X509 encoded
   *
   * @param bytes
   * @return
   */
  public createPublicKeyFromBytes(bytes: Uint8Array): any {
    throw new Error('Method not implemented.');
  }
}
