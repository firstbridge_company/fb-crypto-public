/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import * as asn1js from 'asn1js';
import { arrayBufferToString, toBase64 } from 'pvutils';
import CryptoParams from '../../CryptoParams';
import X509CertOperations from '../../csr/X509CertOperations';
import CertificateRequestData from '../../csr/CertificateRequestData';

const {
  CertificationRequest,
  Attribute,
  Extensions,
  Extension,
  CryptoEngine,
  setEngine,
  Certificate,
  CertificateChainValidationEngine,
} = require('pkijs');
const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();
setEngine('newEngine', crypto, new CryptoEngine({ name: '', crypto, subtle: crypto.subtle }));

export default class X509CertOperationsImpl implements X509CertOperations {
  private params: CryptoParams;

  constructor(params: CryptoParams) {
    this.params = params;
  }

  public async createSelfSignedX509v3(
    kp: CryptoKeyPair,
    certData: CertificateRequestData,
  ): Promise<{
    ['certificate']: any;
    ['certificatePem']: string;
    ['privateKeyPem']: string;
    ['publicKeyPem']: string;
  }> {
    let certificateBuffer = new ArrayBuffer(0);
    const certificate = new Certificate();
    certificate.serialNumber = new asn1js.Integer({ value: Date.now() });
    certificate.version = 2; //must be 2 to indicate version 3, they count from 0
    // yesterday
    const validityBeginDate: number = new Date().valueOf() - 24 * 60 * 60 * 1000;
    // in 2 years
    const validityEndDate: number = new Date().valueOf() + 2 * 365 * 24 * 60 * 60 * 1000;
    certificate.notBefore.value = new Date(validityBeginDate);
    certificate.notAfter.value = new Date(validityEndDate);
    certificate.issuer.typesAndValues.push(...certData.subject);
    certificate.subject.typesAndValues.push(...certData.subject);
    await certificate.subjectPublicKeyInfo.importKey(kp.publicKey);
    certificate.extensions = [];
    certificate.extensions.push(...certData.extensions);

    try {
      await certificate.sign(kp.privateKey, this.params.digester);
    } catch (ex) {
      console.error(`Error during exporting public key: ${ex}`);
    }

    try {
      certificateBuffer = certificate.toSchema(true).toBER(false);
    } catch (ex) {
      console.error(`Error signing PKCS#10: ${ex}`);
    }

    let exportedPrivateKey;
    try {
      exportedPrivateKey = await crypto.subtle.exportKey('pkcs8', kp.privateKey);
    } catch (ex) {
      console.error(`Error during exporting of private key: ${ex}`);
    }

    let exportedPublicKey;
    try {
      exportedPublicKey = await crypto.subtle.exportKey('spki', kp.publicKey);
    } catch (ex) {
      console.error(`Error during exporting of public key: ${ex}`);
    }

    return {
      certificate: certificate,
      certificatePem: this.arrayBufferToPem(certificateBuffer),
      privateKeyPem: this.arrayBufferToPem(exportedPrivateKey, 'PRIVATE KEY'),
      publicKeyPem: this.arrayBufferToPem(exportedPublicKey, 'PUBLIC KEY'),
    };
  }

  public async createX509CertificateRequest(
    kp: CryptoKeyPair,
    certData: CertificateRequestData,
    challengePassword: string,
  ): Promise<{
    ['certificateRequest']: any;
    ['certificateRequestPem']: string;
    ['privateKeyPem']: string;
    ['publicKeyPem']: string;
  }> {
    let pkcs10Buffer = new ArrayBuffer(0);
    const pkcs10 = new CertificationRequest();

    pkcs10.version = 3;
    pkcs10.subject.typesAndValues.push(...certData.subject);
    pkcs10.attributes = [];
    await pkcs10.subjectPublicKeyInfo.importKey(kp.publicKey);
    const subjectKeyIdentifier = await crypto.subtle.digest(
      { name: this.params.digester },
      pkcs10.subjectPublicKeyInfo.subjectPublicKey.valueBlock.valueHex,
    );

    const extensions = [
      new Extension({
        extnID: '2.5.29.14',
        critical: false,
        extnValue: new asn1js.OctetString({ valueHex: subjectKeyIdentifier }).toBER(false),
      }),
      ...certData.extensions,
    ];
    if (challengePassword != null && !challengePassword) {
      const password = new asn1js.PrintableString({ value: challengePassword }).toBER(false);
      extensions.push(
        new Extension({
          extnID: '1.2.840.113549.1.9.7', //pkcs_9_at_challengePassword
          critical: false,
          extnValue: password,
        }),
      );
    }
    pkcs10.attributes.push(
      new Attribute({
        type: '1.2.840.113549.1.9.14', // pkcs_9_at_extensionRequest
        values: [new Extensions({ extensions }).toSchema()],
      }),
    );

    try {
      await pkcs10.sign(kp.privateKey, this.params.digester);
    } catch (ex) {
      console.error(`Error during exporting public key: ${ex}`);
    }

    try {
      pkcs10Buffer = pkcs10.toSchema(true).toBER(false);
    } catch (ex) {
      console.error(`Error signing PKCS#10: ${ex}`);
    }

    let exportedPrivateKey;
    try {
      exportedPrivateKey = await crypto.subtle.exportKey('pkcs8', kp.privateKey);
    } catch (ex) {
      console.error(`Error during exporting of private key: ${ex}`);
    }

    let exportedPublicKey;
    try {
      exportedPublicKey = await crypto.subtle.exportKey('spki', kp.publicKey);
    } catch (ex) {
      console.error(`Error during exporting of public key: ${ex}`);
    }

    return {
      certificateRequest: pkcs10,
      certificateRequestPem: this.arrayBufferToPem(pkcs10Buffer, 'CERTIFICATE REQUEST'),
      privateKeyPem: this.arrayBufferToPem(exportedPrivateKey, 'PRIVATE KEY'),
      publicKeyPem: this.arrayBufferToPem(exportedPublicKey, 'PUBLIC KEY'),
    };
  }

  public async verifyCertificate(trustedCertificates: any[], certificates: any[], crls: any[]): Promise<boolean> {
    const certChainVerificationEngine = new CertificateChainValidationEngine({
      trustedCerts: trustedCertificates,
      certs: certificates,
      crls,
    });
    try {
      const verifyResult = await certChainVerificationEngine.verify();
      if (!verifyResult.result && verifyResult.resultMessage) {
        throw verifyResult.resultMessage;
      }
      return verifyResult.result;
    } catch (ex) {
      throw new Error(`Error during verification:\n${ex}`);
    }
  }

  public arrayBufferToPem(dataBuffer: ArrayBuffer, certName: string = 'CERTIFICATE'): string {
    let resultString = `-----BEGIN ${certName}-----\r\n`;
    resultString = `${resultString}${this.formatPEM(toBase64(arrayBufferToString(dataBuffer)))}`;
    resultString = `${resultString}\r\n-----END ${certName}-----\r\n`;
    return resultString;
  }

  public formatPEM(pemString: string): string {
    const PEM_STRING_LENGTH = pemString.length,
      LINE_LENGTH = 64;
    const wrapNeeded = PEM_STRING_LENGTH > LINE_LENGTH;

    if (wrapNeeded) {
      let formattedString = '',
        wrapIndex = 0;
      for (let i = LINE_LENGTH; i < PEM_STRING_LENGTH; i += LINE_LENGTH) {
        formattedString += pemString.substring(wrapIndex, i) + '\r\n';
        wrapIndex = i;
      }
      formattedString += pemString.substring(wrapIndex, PEM_STRING_LENGTH);
      return formattedString;
    } else {
      return pemString;
    }
  }
}
