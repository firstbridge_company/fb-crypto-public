/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Ring-like buffer for deterministic key generation
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */

export default class NotRandom {
  private seed!: Uint8Array;

  public nextBytes(bytes: Uint8Array): void {
    let idx: number = 0;
    for (let i = 0; i < bytes.length; i++) {
      bytes[i] = this.seed[idx];
      idx++;
      if (idx >= this.seed.length) {
        idx = 0;
      }
    }
  }

  public setSeed(seed: Uint8Array): void {
    this.seed = seed;
  }
}
