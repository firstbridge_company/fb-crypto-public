/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import Cryptor from './Cryptor';
import AsymKeysHolder from './AsymKeysHolder';

/**
 * Integrated encryption scheme (IES) encryption routines. For ECC: ECIES is
 * used that calculates shared key using some variant of ECDH. Rot RSA: simple
 * encryption with limitation of length
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface AsymCryptor extends Cryptor {
  /**
   * Set all required keys
   *
   * @param keys our public and private keys, their public key
   */
  setKeys(keys: AsymKeysHolder): void;
}
