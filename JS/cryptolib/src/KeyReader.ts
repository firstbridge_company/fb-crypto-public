/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import Certificate from 'pkijs/src/Certificate';

/**
 * Reads keys in various formats and returns in the form acceptable by FBCrypto
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface KeyReader {
  extractPublicKeyFromX509(c: any): Promise<CryptoKey>;

  readEncryptedPrivateKeyPEM(input: any, password: string): any;

  readX509CertPEMorDER(is: string): Certificate;

  readPKCS12File(path: string, password: string, alias: string): CryptoKeyPair;

  readPrivateKeyPEM(input: string): Promise<CryptoKey>;

  readPrivateKeyPKCS12(PKCS12filePath: string, password: string, keyPassword: string, alias: string): any;

  readPrivateKeyPKCS8(input: any): any;

  readPublicKeyPKCS12(PKCS12filePath: string, password: string, alias: string): any;

  /**
   * Reads private key from most standard and compatible representation (ASN.1 in PKSC#8)
   * Compatible with OpenSSL.
   *
   * @param keyBytes bytes of private key
   * @return re-constructed private key
   */
  deserializePrivateKey(keyBytes: Uint8Array): CryptoKey;

  /**
   * Reads public key from most standard and compatible representation (ASN.1 in X509)
   * Compatible with OpenSSL.
   *
   * @param keyBytes bytes of public key
   * @return re-constructed public key
   */
  deserializePublicKey(keyBytes: Uint8Array): CryptoKey;
}
