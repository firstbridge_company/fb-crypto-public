/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

export default class ElGamalEncryptedMessage {
  private _M1: any;
  private _M2: any;

  get M2(): any {
    return this._M2;
  }

  set M2(value: any) {
    this._M2 = value;
  }

  get M1(): any {
    return this._M1;
  }

  set M1(value: any) {
    this._M1 = value;
  }
}
