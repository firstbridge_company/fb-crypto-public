/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Class that represents decryption result of AEAD (unencrypted part) and encrypted part of message
 */
export default class AEADPlain {
  /**
   * plain part of AEAD message
   */
  private _plain!: Uint8Array;
  /**
   * decrypted part of AEAD message
   */
  private _decrypted!: ArrayBuffer;
  /**
   * indicator of correctness
   */
  private _hmacOk!: boolean;

  get hmacOk(): boolean {
    return this._hmacOk;
  }

  set hmacOk(value: boolean) {
    this._hmacOk = value;
  }

  get decrypted(): ArrayBuffer {
    return this._decrypted;
  }

  set decrypted(value: ArrayBuffer) {
    this._decrypted = value;
  }

  get plain(): Uint8Array {
    return this._plain;
  }

  set plain(value: Uint8Array) {
    this._plain = value;
  }
}
