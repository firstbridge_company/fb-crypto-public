/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

const BYTES: number = 4;

export default class ByteBuffer {
  private _position: number = 0;
  private readonly bufferArr: Uint8Array;

  public constructor(bufferArr: Uint8Array) {
    this.bufferArr = bufferArr;
  }

  public static allocate(capacity: number): Uint8Array {
    return new Uint8Array(capacity);
  }

  public get(length: number): Uint8Array {
    const result = this.bufferArr.slice(this.position, this.position + length);
    this.position += length;
    return result;
  }

  public getInt(): number {
    const view = new DataView(this.bufferArr.buffer);
    const result = view.getUint32(this.position);
    this.position += BYTES;
    return result;
  }

  get position(): number {
    return this._position;
  }

  set position(value: number) {
    this._position = value;
  }
}
