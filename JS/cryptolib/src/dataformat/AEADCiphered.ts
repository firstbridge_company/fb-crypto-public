/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from '../CryptoParams';
import ByteBuffer from './ByteBuffer';

/**
 * Defines message format for AEAD with IV, plain authenticated
 * data and encrypted data.
 * specially formated data that includes:
 *    IV  (12 bytes), (salt+explicit nonce)
 *    unencryped data lenght (4 bytes),
 *    ecnrypted data lenght (4 bytes)
 *    unencrypted data (variable len),
 *    encrypted data in the rest of message including
 *    last 16 bytes (128 bits) of hmac
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class AEADCiphered {
  /**
   * Maximal size of plain and encrypted parts in sum to prevent DoS attacks
   */
  public static MAX_MSG_SIZE: number = 65536;

  private _aatext: Uint8Array = new Uint8Array(0);
  private _encrypted: Uint8Array = new Uint8Array(0);
  private _hmacSize: number;
  private _iv: Uint8Array; // 12 bytes = 4 of salt + 8 of nonce

  private cryptoParams: CryptoParams;
  private BYTES: number = 4;

  public constructor(cryptoParams: CryptoParams) {
    this.cryptoParams = cryptoParams;
    this._hmacSize = cryptoParams.gcmAuthTagLenBits / 8; // 128 bits
    this._iv = new Uint8Array(cryptoParams.symIvLen); // 12 bytes, RFC 5288;  salt and explicit nonce
  }

  /**
   * Sets 8 bytes of implicit part on nonce that goes with message
   * @param en 8 bytes of explicit part of IV
   */
  public setExplicitNonce(en: Uint8Array): void {
    if (en.byteLength !== 8) {
      throw new Error('Nonce size must be exactly 8 bytes');
    }
    this.iv = new Uint8Array(12);
    this.iv.set(en, 4);
  }

  public getExplicitNonce(): Uint8Array {
    return this.iv.slice(4, 12);
  }

  get iv(): Uint8Array {
    return this._iv;
  }

  set iv(value: Uint8Array) {
    if (value.byteLength !== this.cryptoParams.symIvLen) {
      throw new Error('Nonce size must be exactly 8 bytes');
    }
    this._iv = value;
  }

  public getHMAC(): Uint8Array {
    return this._encrypted.slice(this._encrypted.byteLength - this._hmacSize - 1, this._encrypted.byteLength - 1);
  }

  public static fromBytes(message: Uint8Array, cryptoParams: CryptoParams): AEADCiphered {
    const res: AEADCiphered = new AEADCiphered(cryptoParams);
    const bb = new ByteBuffer(message);
    res.iv = bb.get(res.iv.byteLength);
    const txtlen: number = bb.getInt();
    const enclen: number = bb.getInt();

    // prevent overflow attack
    if (txtlen + enclen > this.MAX_MSG_SIZE) {
      throw new Error('Declared message size is too big: ' + (txtlen + enclen));
    }
    res._aatext = bb.get(txtlen);
    res._encrypted = bb.get(enclen);
    return res;
  }

  public toBytes(): Uint8Array {
    const capacity: number = this.calcBytesSize();
    const bb: Uint8Array = ByteBuffer.allocate(capacity);
    bb.set(this.iv);
    bb.set(this._aatext, this.iv.byteLength + this.BYTES + this.BYTES);
    bb.set(this._encrypted, this.iv.byteLength + this.BYTES + this.BYTES + this._aatext.byteLength);
    const view = new DataView(bb.buffer);
    view.setUint32(this.iv.byteLength, this._aatext.byteLength);
    view.setUint32(this.iv.byteLength + this.BYTES, this._encrypted.byteLength);
    return new Uint8Array(view.buffer);
  }

  public calcBytesSize(): number {
    return this._iv.byteLength + this.BYTES + this.BYTES + this._aatext.byteLength + this._encrypted.byteLength;
  }

  get aatext(): Uint8Array {
    return this._aatext;
  }

  set aatext(value: Uint8Array) {
    this._aatext = value;
  }

  get encrypted(): Uint8Array {
    return this._encrypted;
  }

  set encrypted(value: Uint8Array) {
    this._encrypted = value;
  }

  get hmacSize(): number {
    return this._hmacSize;
  }

  set hmacSize(value: number) {
    this._hmacSize = value;
  }
}
