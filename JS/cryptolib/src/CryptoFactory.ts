/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import KeyReader from './KeyReader';
import KeyWriter from './KeyWriter';
import CryptoParams from './CryptoParams';
import CryptoConfig from './CryptoConfig';
import SymCryptor from './SymCryptor';
import SymJCEImpl from './impl/ecc/SymJCEImpl';
import JCEDigestImpl from './impl/JCEDigestImpl';
import KeyReaderImpl from './impl/KeyReaderImpl';
import KeyWriterImpl from './impl/KeyWriterImpl';
import AsymCryptorDH from './AsymCryptorDH';
import AsymJCEECDHImpl from './impl/ecc/AsymJCEECDHImpl';
import AsymCryptor from './AsymCryptor';
import AsymJCEIESImpl from './impl/ecc/AsymJCEIESImpl';
import CryptoSignature from './CryptoSignature';
import CryptoSignatureImpl from './impl/CryptoSignatureImpl';
import ElGamalCrypto from './ElGamalCrypto';
import ElGamalCryptoImpl from './impl/ecc/ElGamalCryptoImpl';
import Digester from './Digester';
import KeyGenerator from './KeyGenerator';
import KeyGeneratorEC from './impl/ecc/KeyGeneratorEC';
import X509CertOperations from './csr/X509CertOperations';
import AsymCryptorRSAImpl from './impl/rsa/AsymCryptorRSAImpl';
import KeyGeneratorRSA from './impl/rsa/KeyGeneratorRSA';
import X509CertOperationsImpl from './impl/csr/X509CertOperationsImpl';

/**
 * Factory that creates configured implementations of FBCrypto interfaces. These
 * all crypto routines that could be used with any supported encryption system
 * defined in FbCryptoParams,
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class CryptoFactory {
  private readonly params: CryptoParams;

  constructor(p: CryptoParams) {
    this.params = p;
  }

  /**
   * Creates instance of factory with parameters
   *
   * @param p set of crypto parameters, @see CryptoParams
   * @return ready to use factory with defined parameter set
   */
  public static newInstance(p?: CryptoParams): CryptoFactory {
    return p ? new CryptoFactory(p) : new CryptoFactory(CryptoConfig.createDefaultParams());
  }

  /**
   * Instantiates symmetrical crypto routines with agreed parameters
   *
   * @return symmetrical crypto routines instance
   */
  public getSymCryptor(): SymCryptor {
    return new SymJCEImpl(this.params);
  }

  /**
   * Instantiates routines for data encryption data using Elliptic Curves
   * Diffie-Hellman key agreement and agreed AES-128 or AES-256 encryption. It
   * is possible to use one-step ECDH using pre-defined keys or 2-step ECDHE
   * on Ephemeral keys. Ephemeral means that key pairs are created from random
   * seed on the fly, common key produced from them and then keys being thrown
   * away. This is most secure scheme but requires 2 step of key exchange
   *
   * @return instance of crypto routines with agreed parameters
   *
   */
  public getAsymCryptorDH(): AsymCryptorDH {
    return new AsymJCEECDHImpl(this.params);
  }

  /**
   * Creates instance of crypto routines for Integrated Encryption Scheme.
   * Some default ECC IES (see Bouncy Castle library) is used for ECC
   * cryptography. With ECC size of message is not limited. In case of RSA
   * cryptography default RSA encryption scheme is used. Note, that size of
   * message is very limited in that case.
   *
   * @return
   */
  public getAsymCryptor(): AsymCryptor {
    if ('EC' === this.params.signatureSchema) {
      return new AsymJCEIESImpl(this.params);
    } else {
      return new AsymCryptorRSAImpl(this.params);
    }
  }

  /**
   * Creates instance of signer and signature verificator for defined
   * encryption scheme
   *
   * @return signer/verifier instance
   */
  public getCryptoSiganture(): CryptoSignature {
    return new CryptoSignatureImpl(this.params);
  }

  /**
   * Creates instance of ElGammal procedures
   *
   * @return ElGammal procedures instance
   */
  public getElGamalCrypto(): ElGamalCrypto {
    return new ElGamalCryptoImpl(this.params);
  }

  /**
   * Creates instance of various digesters. Default digester could be
   * different for different values of FBCryptoParans. It is optimized by
   * security and performance. But anyway any implemented digester is
   * available.
   *
   * @return
   */
  public getDigesters(): Digester {
    return new JCEDigestImpl(this.params);
  }

  /**
   * Creates key reader instance, able to read keys in different formats.
   * Usually, key reader does not depend on encryption Schema
   *
   * @return Key reader instance
   */
  public getKeyReader(): KeyReader {
    return new KeyReaderImpl();
  }

  /**
   * Creates key write instance, able to write keys in different formats.
   * Usually, key rwriter does not depend on encryption Schema
   *
   * @return Key reader instance
   */
  public getKeyWriter(): KeyWriter {
    return new KeyWriterImpl();
  }

  /**
   * Creates instance of key generator.
   *
   * @return Key generator for chosen crypto scheme
   */
  public getKeyGenerator(): KeyGenerator {
    if ('EC' === this.params.signatureSchema) {
      return new KeyGeneratorEC(this.params);
    } else {
      //RSA
      return new KeyGeneratorRSA(this.params);
    }
  }

  public getX509CertOperations(): X509CertOperations {
    return new X509CertOperationsImpl(this.params);
  }

  /**
   * Crypto parameters that are in use of current CryptoFactory instance
   * @return Crypto parameters that are in use
   */
  public getCryptoParams(): CryptoParams {
    return this.params;
  }
}
