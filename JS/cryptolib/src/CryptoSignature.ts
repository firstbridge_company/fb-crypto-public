/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import AsymKeysHolder from './AsymKeysHolder';

/**
 * Interface to signature operations
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface CryptoSignature {
  /**
   * Set all required keys
   * @param keys our public and private keys, their public key
   */
  setKeys(keys: AsymKeysHolder): void;

  /**
   * Sign message using private key.Please be careful while constructing FPCryptoParams,
   * pay attention to signature algorithm inside of it
   * @param message input message. No matter encrypted or not.
   * @return signature bytes ( In ASN1 format, encoding - DER)
   */
  sign(message: Uint8Array): Promise<Uint8Array>;

  /**
   * Verifies signature using theirPublicKey
   * Signature algorithm depends on FBCryptoParams settings.
   * Please construct crypto routines
   * using CryptoMetaFactory from appropriate signature method ID string
   * @param message message bytes
   * @param signature signature bytes (In ASN1 format. Encoding - DER)
   * @return true if message is authentic false otherwise
   */
  verify(message: Uint8Array, signature: Uint8Array): Promise<boolean>;

  /**
   * Sign message using private key.Please be careful while constructing FPCryptoParams,
   * pay attention to signature algorithm inside of it
   * @param message input message. No matter encrypted or not.
   * @return signature bytes as concatenation of R and S binary vectors
   * (2 * 66 = 132 bytes)
   */
  signPlain(message: Uint8Array): Promise<Uint8Array>;

  /**
   * Verifies signature using theirPublicKey
   * Signature algorithm depends on FBCryptoParams settings.
   * Please construct cryptographic routines
   * using CryptoMetaFactory from appropriate signature method ID string
   * @param message message bytes
   * @param signature signature bytes ( As concatenation of R and S, 132 bytes)
   * @return true if message is authentic false otherwise
   */
  verifyPlain(message: Uint8Array, signature: Uint8Array): Promise<boolean>;
}
