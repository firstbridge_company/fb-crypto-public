/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import Cryptor from './Cryptor';

/**
 * Interface to symmetric crypto routines
 *
 *  @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 * REFERENCES: [1]
 * https://tools.ietf.org/rfc/rfc5288.txt AES Galois Counter Mode (GCM) Cipher
 * Suites for TLS [2] https://tools.ietf.org/html/draft-ietf-tls-tls13-28 TLS
 * 1.3 draft [3] https://tools.ietf.org/html/rfc5246 TLS 1.2 [4]
 * https://tools.ietf.org/html/rfc5116 An Interface and Algorithms for
 * Authenticated Encryption [5] https://tools.ietf.org/html/rfc4106 The Use of
 * Galois/Counter Mode (GCM) in IPsec Encapsulating Security Payload (ESP) [6]
 * https://tools.ietf.org/html/rfc7748 Elliptic Curves for Security
 */
export default interface SymCryptor extends Cryptor {
  /**
   * Set key for AES/GCM which is used for symmetrical encryption.
   *
   * @param key then 128 or 256 bits of key   0x67, 0xba, 0x05, 0x10, 0x26, 0x2a, 0xe4, 0x87, 0xd7, 0x37, 0xee, 0x62,
   0x98, 0xf7, 0x7e, 0x0c
   */
  setKey(key: Uint8Array): void;

  /**
   *
   * @param IV Initialization vector variable part, 4+8=12 bytes, or salt and
   * explicit_nonce used to init GCM. So it could be 4 bytes of "fixed"
   * nonce or full 12 bytes. In case of 4 bytes random 8 bytes generated for
   * nonce_explicit From RFC 5288: AES-GCM security requires that the counter
   * is never reused. The IV construction in Section 3 is designed to prevent
   * counter reuse. Implementers should also understand the practical
   * considerations of IV handling outlined in Section 9 of [GCM]. In this
   * class IV is 12 bytes as defined in RFC 5116 struct { opaque salt[4];
   * opaque nonce_explicit[8]; } GCMNonce; Salt is "fixed" part of IV and
   * comes with key, nonce_explicit is "variable" part of IV and comes with
   * message. So IV in this method should be 12 bytes long
   */
  setIV(IV: Uint8Array): void;

  /**
   * 12 bytes of salt + nonce
   *
   * @return IV consisting of salt and nonce
   */
  getIV(): Uint8Array;

  /**
   * Set 4 bytes of salt, fixed part of GCM IV
   *
   * @param salt 4 bytes array
   */
  setSalt(salt: Uint8Array): void;

  /**
   * set 8 bits of variable part of GCM IV
   *
   * @param explicit_nonce 8 bit array of nonce if explicit_nonce is null
   * random value is generated
   */
  setNonce(explicit_nonce: Uint8Array | null): void;

  /**
   * get 8 bytes of variable part of GCM IV
   *
   * @return b bytes array
   */
  getNonce(): Uint8Array;

  getSalt(): Uint8Array;
}
