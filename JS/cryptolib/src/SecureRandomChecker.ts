/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Check secure random generation speed. It should be fast enough
 * to generate keys quickly. On Linux systems package
 * should be installed to update /dev/random with enough entropy
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface SecureRandomChecker {
  // public static readonly SECURE_RANDOM_ACCEPTABLE_TIME_MS: number = 50L;

  check(): boolean;
}
