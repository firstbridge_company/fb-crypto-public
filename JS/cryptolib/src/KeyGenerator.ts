/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

let crypto: any;
if (typeof window === 'undefined' || typeof window.crypto === 'undefined') {
  const { Crypto } = require('node-webcrypto-ossl');
  crypto = new Crypto();
} else {
  crypto = window.crypto;
}

/**
 * Key generator with defined key parameters
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface KeyGenerator {
  /**
   * Generate ECDSA PublicKey X509 encoded
   *
   * @param bytes
   * @return
   */
  createPublicKeyFromBytes?(bytes: Uint8Array): CryptoKey;

  /**
   * Simple deterministic key derivation function. It is one-way function. It
   * calculates hash (defined in params) of secretPhrase.getBytes() and salt.
   * If there is not enough bytes (keyLen) it uses hash result and the same
   * salt agian and puts additional bytes to output.
   *
   * @param secretPhrase UTF-8 encoded string
   * @param salt random salt at least of 16 bytes
   * @param keyLenBits desired output length in bits (should be byte-aligned)
   * @return array of bytes that is determined by secretPhrase ans salt. It is
   * hard to calculate secretPhrase from it because it uses string
   * cryptographic hashing function SHA-512
   */
  deriveFromSecretPhrase(secretPhrase: string, salt: Uint8Array, keyLenBits: number): Promise<Uint8Array>;

  /**
   * Generate deterministic ECC key pair using defaultCurve and
   * passphrase. Well, obviously all the security depends on randomness of
   * passphrase!
   *
   * @param secretPhrase long enough and random enough pass phrase. You've
   *                     been warned!
   * @param salt         some random number, recommended size is 16 bytes
   * @return EEC key pair
   */
  generateKeys(secretPhrase?: string, salt?: Uint8Array): Promise<CryptoKeyPair>;

  generateSymKey(): Uint8Array;

  generateIV(): Uint8Array;
}
