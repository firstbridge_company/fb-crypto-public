/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import ElGamalKeyPair from './ElGamalKeyPair';

/**
 * ElGamal procedures
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface ElGamalCrypto {
  /**
   * Decrypt ElGamalEncryptedMessage sent to us using our private key
   *
   * @param priKey our private key
   * @param cryptogram formatted message parsed into ElGamalEncryptedMessage
   * @return decryption result, could be interpreted as byte array
   */
  decrypt(priKey: CryptoKey, cryptogram: any): any;

  /**
   * ElGamal Encryption routine using 2 components of public key
   *
   * @param publicKeyX x coordinate of public key, presented as BigInteger
   * @param publicKeyY y coordinate of public key, presented as BigInteger
   * @param plainText  plain text in format of BigInteger
   * @return ElGamalEncryptedMessage crypto container, consisting
   * of M1 as ECPoint with X,Y coordinates and M2 as BigInteger
   */
  encrypt(publicKeyX: any, publicKeyY: any, plainText: any): any;

  /**
   * ElGamal Keys generation routine
   *
   * @return ElGamalKeyPair crypto container, consisting
   * of as public key as ECPoint with X,Y coordinates and
   * private key as BigInteger
   */
  generateOwnKeys(): ElGamalKeyPair;

  /**
   * ElGamal getter for public key abscissa
   *
   * @return x coordinate of public as BigInteger
   */
  getPublicKeyX(): any;

  /**
   * ElGamal getter for public key ordinate
   *
   * @return y coordinate of public as BigInteger
   */
  getPublicKeyY(): any;

  /**
   * ElGamal getter for private key
   *
   * @return private key as BigInteger
   */
  getPrivateKey(): any;
}
