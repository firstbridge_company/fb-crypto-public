/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Write keys in various formats
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default interface KeyWriter {
  addX509CertToPKCS12(certificate: any, pathToJKS: string, alias: string, jksPassword: string): any;

  serializePrivateKey(privateKey: CryptoKey): Promise<Uint8Array>;

  serializePublicKey(publicKey: CryptoKey): Promise<Uint8Array>;

  writeCertificateRequestPEM(path: string, cr: any): void;

  writePvtKeyPEM(path: string, key: CryptoKey): void;

  getPvtKeyPEM(key: CryptoKey): string;

  writePvtKeyPKCS12(path: string): void;

  writeX509CertificatePEM(path: string, certificate: any): void;

  getX509CertificatePEM(certificate: any): string;

  getCertificateRequestPEM(cr: any): string;
}
