import * as fs from 'fs';
import KeyReaderImpl from '../impl/KeyReaderImpl';
import KeyWriterImpl from '../impl/KeyWriterImpl';
import CryptoConfig from '../CryptoConfig';
import KeyGeneratorEC from '../impl/ecc/KeyGeneratorEC';

const CERT_FILE = '../../../../testdata/cert-ecc/cacert.pem';
const params = CryptoConfig.createDefaultParams();
let kg;
let kp;

beforeAll(async () => {
  kg = new KeyGeneratorEC(params);
  kp = await kg.generateKeys();
});

test('PrivateKey Serialization', async () => {
  const kw = new KeyWriterImpl();
  const kr = new KeyReaderImpl();
  // const s = kw.serializePrivateKey(kp.privateKey);
  // const pk = kr.deserializePrivateKey(s);
  // const privateKeyDeserializeExported: ArrayBuffer = await webcrypto.subtle.exportKey("raw", pk);
  // const privateKeyExported: ArrayBuffer = await webcrypto.subtle.exportKey("raw", kp.privateKey);

  // console.log(privateKeyExported)
  // console.log(privateKeyDeserializeExported)
  // expect(privateKeyDeserializeExported).toEqual(privateKeyExported);
});

test('Read PEM', () => {
  expect.assertions(2);
  const kr = new KeyReaderImpl();
  const is = fs.readFileSync(`${__dirname}/${CERT_FILE}`, 'binary');
  expect(is).not.toBeNull();
  const cert = kr.readX509CertPEMorDER(is);
  expect(cert).not.toBeNull();
});
