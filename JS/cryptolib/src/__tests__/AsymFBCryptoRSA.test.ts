import * as fs from 'fs';
import { arrayBufferToString, stringToArrayBuffer } from 'pvutils';
import KeyReaderImpl from '../impl/KeyReaderImpl';
import CryptoParams from '../CryptoParams';
import AsymKeysHolder from '../AsymKeysHolder';
import KeyPair from '../KeyPair';
import CryptoConfig from '../CryptoConfig';
import CryptoSignatureImpl from '../impl/CryptoSignatureImpl';
import AsymCryptorRSAImpl from '../impl/rsa/AsymCryptorRSAImpl';

const kr = new KeyReaderImpl();
const kpAlice = new KeyPair();
const kpBob = new KeyPair();
const TEST_DATA_DIR = '../../../../testdata/';
const PLAIN_FILE = 'input/lorem_ipsum.txt';
const SIGNATURE_ALICE_FILE = 'rsa_encrypt_asym_test_signature_alice.bin';
const SIGNATURE_ALICE_FILE_JS = 'js_rsa_encrypt_asym_test_signature_alice.bin';
const OUT_FILE_ENCRYPT_ASYM_ALICE = 'rsa_encrypt_asym_test.bin';
const OUT_FILE_ENCRYPT_ASYM_ALICE_JS = 'js_rsa_encrypt_asym_test.bin';

const CERT_1 = 'cert-rsa/cert_1.pem';
const KEY_1 = 'cert-rsa/key_1_nopass.pem';
const CERT_2 = 'cert-rsa/cert_2.pem';
const KEY_2 = 'cert-rsa/key_2_nopass.pem';
const RSA_KEY_LEN = 4096;

let khA: AsymKeysHolder;
let khB: AsymKeysHolder;

const writeToFile = (data: Uint8Array, fileName: string) => {
  const dataText = arrayBufferToString(data);
  fs.writeFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, dataText, { encoding: 'binary', flag: 'w' });
};

const readFromFile = (fileName: string, size?: number) => {
  const file = fs.readFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, 'binary');
  if (size && size < file.length) {
    return file.substr(0, size);
  }
  return file;
};

const params: CryptoParams = CryptoConfig.createRSAn(RSA_KEY_LEN);
const plainArray: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(PLAIN_FILE)));

function compareToFile(data: ArrayBuffer | Uint8Array, fileName: string): boolean {
  const b_file = new Uint8Array(stringToArrayBuffer(readFromFile(fileName)));
  const b_data = new Uint8Array(data);
  if (b_file.byteLength !== b_data.byteLength) return false;
  for (let i = 0; i < b_data.byteLength; i++) {
    if (b_file[i] !== b_data[i]) return false;
  }
  return true;
}

const setKeys = async (Alice: KeyPair, Bob: KeyPair) => {
  const test1Cert = kr.readX509CertPEMorDER(readFromFile(CERT_1));
  Alice.privateKey = await kr.readPrivateKeyPEM(readFromFile(KEY_1));
  Alice.publicKey = await kr.extractPublicKeyFromX509(test1Cert);

  const test2Cert = kr.readX509CertPEMorDER(readFromFile(CERT_2));
  Bob.privateKey = await kr.readPrivateKeyPEM(readFromFile(KEY_2));
  Bob.publicKey = await kr.extractPublicKeyFromX509(test2Cert);
};

describe('Asymmetric Key Algorithms', () => {
  beforeAll(async () => {
    // Reading certificates and keys for asymmetric crypto tests
    await setKeys(kpAlice, kpBob);
    khA = new AsymKeysHolder(kpAlice.publicKey, kpAlice.privateKey, kpBob.publicKey);
    khB = new AsymKeysHolder(kpBob.publicKey, kpBob.privateKey, kpAlice.publicKey);
  });

  test('Sign', async () => {
    expect.assertions(3);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new CryptoSignatureImpl(params);
    await instance1.setKeys(khA);
    const signature = await instance1.sign(plainArray);

    // 1) JS: Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new CryptoSignatureImpl(params);
    await instance2.setKeys(khB);

    const res = await instance2.verify(plainArray, signature);
    expect(res).toBe(true);

    // 2) Java: Bob generates the same secret key using his private key and Alice's public key.
    const signJava = new Uint8Array(stringToArrayBuffer(readFromFile(SIGNATURE_ALICE_FILE)));
    const instance2Java = new CryptoSignatureImpl(params);
    await instance2Java.setKeys(khB);
    const resJava = await instance2Java.verify(plainArray, signJava);

    writeToFile(signature, SIGNATURE_ALICE_FILE_JS);

    expect(resJava).toBe(true);

    // check with JAVA result
    expect(compareToFile(signature, SIGNATURE_ALICE_FILE)).toBe(true);
  });

  test('EncryptAsymmetricIES', async () => {
    // expect.assertions(2);
    const encodedOurPublicKey = await khA.getEncoded(khA.ourPublicKey);
    const keyLenBytes: number = encodedOurPublicKey.byteLength - 38; // 38 bytes is ASN.1 encoded types
    const maxMessageSize: number = keyLenBytes - 11; // RSA limitations
    const plainArray: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(PLAIN_FILE, maxMessageSize)));

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new AsymCryptorRSAImpl(params);
    await instance1.setKeys(khA);
    const encrypted = await instance1.encrypt(plainArray);

    // no need to write file
    writeToFile(encrypted, OUT_FILE_ENCRYPT_ASYM_ALICE_JS);

    // 1) JS: Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new AsymCryptorRSAImpl(params);
    await instance2.setKeys(khB);
    const decrypted: Uint8Array = await instance2.decrypt(encrypted);
    expect(decrypted).toEqual(plainArray);
    /*
    // 2) Java: Bob generates the same secret key using his private key and Alice's public key.
    const encryptedBufJava: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_ASYM_ALICE)));
    const instance2Java = new AsymJCEECDHImpl(params);
    await instance2Java.setKeys(khB);
    await instance2Java.calculateSharedKey();
    const decryptedJava: Uint8Array = await instance2Java.decrypt(encryptedBufJava);
    expect(decryptedJava).toEqual(plainArray);
    */
  });
});
