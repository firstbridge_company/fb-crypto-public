import * as fs from 'fs';
import { arrayBufferToString, stringToArrayBuffer } from 'pvutils';
import KeyReaderImpl from '../impl/KeyReaderImpl';
import AsymJCEECDHImpl from '../impl/ecc/AsymJCEECDHImpl';
import CryptoParams from '../CryptoParams';
import AsymKeysHolder from '../AsymKeysHolder';
import KeyPair from '../KeyPair';
import CryptoConfig from '../CryptoConfig';
import CryptoSignatureImpl from '../impl/CryptoSignatureImpl';

const kr = new KeyReaderImpl();
const kpAlice = new KeyPair();
const kpBob = new KeyPair();
const TEST_DATA_DIR = '../../../../testdata/';
const PLAIN_FILE_TEXT = 'input/lorem_ipsum.txt';
const SHARED_KEY_ECDHE_FILE = 'ecc_encrypt_asym_test_key_echdhe.bin';
const SHARED_KEY_ECDHE_FILE_JS = 'js_encrypt_asym_test_key_echdhe.bin';
const SHARED_KEY_ECDH_FILE = 'ecc_encrypt_asym_test_key_ecdh.bin';
const SHARED_KEY_ECDH_FILE_JS = 'js_encrypt_asym_test_key_ecdh.bin';
const SIGNATURE_ALICE_FILE = 'ecc_encrypt_asym_test_signature_alice.bin';
const SIGNATURE_ALICE_PLAIN_FILE = 'ecc_encrypt_asym_test_signature_alice_plain.bin';
const SIGNATURE_ALICE_FILE_JS = 'js_encrypt_asym_test_signature_alice.bin';
const OUT_FILE_ENCRYPT_ASYM_ALICE = 'ecc_encrypt_asym_test.bin';
const OUT_FILE_ENCRYPT_ASYM_ALICE_JS = 'js_encrypt_asym_test.bin';
const OUT_FILE_ENCRYPT_ASYM_AEAD_ALICE = 'ecc_encrypt_asym_aead_test.bin';
const OUT_FILE_ENCRYPT_ASYM_AEAD_ALICE_JS = 'js_encrypt_asym_aead_test.bin';
const OPEN_TEXT = '>>>This is test open text. Should be visisble as is<<<';

const CERT_1 = 'cert-ecc/test1_cert.pem';
const KEY_1 = 'cert-ecc/test1_pvtkey.pem';
const CERT_2 = 'cert-ecc/test2_cert.pem';
const KEY_2 = 'cert-ecc/test2_pvtkey.pem';
const RANDOM_BYTES_NUMBER = 4096;

let khA: AsymKeysHolder;
let khB: AsymKeysHolder;

const writeToFile = (data: Uint8Array, fileName: string) => {
  const dataText = arrayBufferToString(data);
  fs.writeFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, dataText, { encoding: 'binary', flag: 'w' });
};

const readFromFile = (fileName: string) => {
  return fs.readFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, 'binary');
};

const params: CryptoParams = CryptoConfig.createDefaultParams();
const plainArray: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(PLAIN_FILE_TEXT)));
const open = OPEN_TEXT;
const openFormatted = new Uint8Array(stringToArrayBuffer(open));

function compareToFile(data: ArrayBuffer | Uint8Array, fileName: string): boolean {
  const b_file = new Uint8Array(stringToArrayBuffer(readFromFile(fileName)));
  const b_data = new Uint8Array(data);
  if (b_file.byteLength !== b_data.byteLength) return false;
  for (let i = 0; i < b_data.byteLength; i++) {
    if (b_file[i] !== b_data[i]) return false;
  }
  return true;
}

const setKeys = async (Alice: KeyPair, Bob: KeyPair) => {
  const test1Cert = kr.readX509CertPEMorDER(readFromFile(CERT_1));
  Alice.privateKey = await kr.readPrivateKeyPEM(readFromFile(KEY_1));
  Alice.publicKey = await kr.extractPublicKeyFromX509(test1Cert);

  const test2Cert = kr.readX509CertPEMorDER(readFromFile(CERT_2));
  Bob.privateKey = await kr.readPrivateKeyPEM(readFromFile(KEY_2));
  Bob.publicKey = await kr.extractPublicKeyFromX509(test2Cert);
};

describe('Asymmetric Key Algorithms', () => {
  beforeAll(async () => {
    // Reading certificates and keys for asymmetric crypto tests
    await setKeys(kpAlice, kpBob);
    khA = new AsymKeysHolder(kpAlice.publicKey, kpAlice.privateKey, kpBob.publicKey);
    khB = new AsymKeysHolder(kpBob.publicKey, kpBob.privateKey, kpAlice.publicKey);
  });

  test('ECDHE', async () => {
    expect.assertions(2);

    console.log('ECDHE keys writing to file: ' + SHARED_KEY_ECDHE_FILE_JS);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new AsymJCEECDHImpl(params);
    khA = new AsymKeysHolder(kpAlice.publicKey, kpAlice.privateKey, kpBob.publicKey);
    await instance1.setKeys(khA);

    // Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new AsymJCEECDHImpl(params);
    khB = new AsymKeysHolder(kpBob.publicKey, kpBob.privateKey, kpAlice.publicKey);
    await instance2.setKeys(khB);

    const aliceSignedKey: Uint8Array = await instance1.ecdheStep1();
    const bobSignedKey: Uint8Array = await instance2.ecdheStep1();

    const sharedKeyAlice: Uint8Array = await instance1.ecdheStep2(bobSignedKey);
    const sharedKeyBob: Uint8Array = await instance2.ecdheStep2(aliceSignedKey);

    expect(sharedKeyAlice).toEqual(sharedKeyBob);
    writeToFile(sharedKeyBob, SHARED_KEY_ECDH_FILE_JS);
    // check with JAVA result
    //TODO: fix compare
    expect(compareToFile(sharedKeyAlice, SHARED_KEY_ECDHE_FILE)).toBe(true);
  });

  test('ECDH', async () => {
    expect.assertions(2);

    console.log('ECDH keys writing to file: ' + SHARED_KEY_ECDH_FILE_JS);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new AsymJCEECDHImpl(params);
    await instance1.setKeys(khA);

    // Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new AsymJCEECDHImpl(params);
    await instance2.setKeys(khB);

    const sharedKeyAlice: Uint8Array = await instance1.calculateSharedKey();
    const sharedKeyBob: Uint8Array = await instance2.calculateSharedKey();

    expect(sharedKeyAlice).toEqual(sharedKeyBob);
    writeToFile(sharedKeyBob, SHARED_KEY_ECDH_FILE_JS);
    // check with JAVA result
    expect(compareToFile(sharedKeyAlice, SHARED_KEY_ECDH_FILE)).toBe(true);
  });

  test('Sign', async () => {
    expect.assertions(2);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new CryptoSignatureImpl(params);
    await instance1.setKeys(khA);
    const signature = await instance1.sign(plainArray);

    // 1) JS: Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new CryptoSignatureImpl(params);
    await instance2.setKeys(khB);

    const res = await instance2.verify(plainArray, signature);
    expect(res).toBe(true);

    // 2) Java: Bob generates the same secret key using his private key and Alice's public key.
    const signJava = new Uint8Array(stringToArrayBuffer(readFromFile(SIGNATURE_ALICE_PLAIN_FILE)));
    const instance2Java = new CryptoSignatureImpl(params);
    await instance2Java.setKeys(khB);
    const resJava = await instance2Java.verify(plainArray, signJava);

    writeToFile(signature, SIGNATURE_ALICE_FILE_JS);

    expect(resJava).toBe(true);

    // check with JAVA result
    // expect(compareToFile(signature, SIGNATURE_ALICE_FILE)).toBe(true);
  });

  test('EncryptDH', async () => {
    expect.assertions(2);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new AsymJCEECDHImpl(params);
    await instance1.setKeys(khA);
    await instance1.calculateSharedKey();
    const encrypted = await instance1.encrypt(plainArray);

    // no need to write file
    writeToFile(encrypted, OUT_FILE_ENCRYPT_ASYM_ALICE_JS);

    // 1) JS: Bob generates the same secret key using his private key and Alice's public key.
    const instance2 = new AsymJCEECDHImpl(params);
    await instance2.setKeys(khB);
    await instance2.calculateSharedKey();
    const decrypted: Uint8Array = await instance2.decrypt(encrypted);
    expect(decrypted).toEqual(plainArray);

    // 2) Java: Bob generates the same secret key using his private key and Alice's public key.
    const encryptedBufJava: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_ASYM_ALICE)));
    const instance2Java = new AsymJCEECDHImpl(params);
    await instance2Java.setKeys(khB);
    await instance2Java.calculateSharedKey();
    const decryptedJava: Uint8Array = await instance2Java.decrypt(encryptedBufJava);
    expect(decryptedJava).toEqual(plainArray);
  });

  test('EncryptAsymmetricWithAEAData', async () => {
    expect.assertions(5);

    // Alice then generates a secret key using her private key and Bob's public key.
    const instance1 = new AsymJCEECDHImpl(params);
    await instance1.setKeys(khA);
    await instance1.calculateSharedKey();
    const encrypted = await instance1.encryptWithAEAData(plainArray, openFormatted);
    const eb: Uint8Array = encrypted.toBytes();
    writeToFile(eb, OUT_FILE_ENCRYPT_ASYM_AEAD_ALICE_JS);

    // 1) JS: Bob generates the same secret key using his private key and Alice's public key.
    const encryptedBuf: Uint8Array = new Uint8Array(
      stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_ASYM_AEAD_ALICE_JS)),
    );
    const instance2 = new AsymJCEECDHImpl(params);
    await instance2.setKeys(khB);
    await instance2.calculateSharedKey();
    const decrypted = await instance2.decryptWithAEAData(encryptedBuf);

    expect(plainArray).toEqual(new Uint8Array(decrypted.decrypted));
    expect(openFormatted).toEqual(decrypted.plain);
    expect(openFormatted).toEqual(encrypted.aatext);

    // 2) Java: Bob generates the same secret key using his private key and Alice's public key.
    const encryptedBufJava: Uint8Array = new Uint8Array(
      stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_ASYM_AEAD_ALICE)),
    );
    const instance2Java = new AsymJCEECDHImpl(params);
    await instance2Java.setKeys(khB);
    await instance2Java.calculateSharedKey();
    const decryptedJava = await instance2Java.decryptWithAEAData(encryptedBufJava);

    expect(plainArray).toEqual(new Uint8Array(decryptedJava.decrypted));
    expect(openFormatted).toEqual(decryptedJava.plain);
  });
});
