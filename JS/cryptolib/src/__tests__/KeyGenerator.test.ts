import CryptoConfig from '../CryptoConfig';
import KeyGeneratorEC from '../impl/ecc/KeyGeneratorEC';
import { bufferToHexCodes } from 'pvutils';

const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

const salt = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);
const params = CryptoConfig.createDefaultParams();
let kg;
let kp;

beforeAll(async () => {
  kg = new KeyGeneratorEC(params);
  kp = await kg.generateKeys();
});

test('GenerateKeys_String', async () => {
  expect.assertions(2);
  const secretPhrase = '1234567890';
  const keyGenerator = new KeyGeneratorEC(params);
  const result: CryptoKeyPair = await keyGenerator.generateKeys(secretPhrase, salt);
  const exportedPublic = await crypto.subtle.exportKey('raw', result.publicKey);
  const pubKeyString = bufferToHexCodes(exportedPublic);
  const exportedPrivate = await crypto.subtle.exportKey('raw', result.privateKey);
  const pvtKeyString = bufferToHexCodes(exportedPrivate);
  expect(pubKeyString.startsWith('30819b301006072a8648ce3d020106052b810400230381860004')).toBe(true);
  expect(pvtKeyString.startsWith('3081f7020100301006072a8648ce3d020106052b810400230481')).toBe(true);
});

test('DeriveFromPasssPhrase', async () => {
  const keyGenerator = new KeyGeneratorEC(params);
  const secretPhrase = '1234567890 and or 0987654321';
  const result = await keyGenerator.deriveFromSecretPhrase(secretPhrase, salt, 256);
  const keyString = bufferToHexCodes(result);
  expect(keyString).toEqual('c5cfb4e7442d4cf37041cca98006cff24b804b4bfff81f73b9d6d359fce1b11b'.toUpperCase());
});

test('CreateX509CertificateRequest', () => {
  // TODO: implement
  console.log('createX509CertificateRequest');
});

test('CreateSerlfSignedX509v3', () => {
  // TODO: implement
  console.log('createSerlfSignedX509v3');
});
