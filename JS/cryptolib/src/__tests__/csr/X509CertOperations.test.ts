import * as fs from 'fs';

import CryptoParams from '../../CryptoParams';
import CryptoConfig from '../../CryptoConfig';
import KeyGenerator from '../../KeyGenerator';
import KeyGeneratorEC from '../../impl/ecc/KeyGeneratorEC';
import X509CertOperations from '../../csr/X509CertOperations';
import X509CertOperationsImpl from '../../impl/csr/X509CertOperationsImpl';
import CertificateRequestData, { SubjectProperties } from '../../csr/CertificateRequestData';
import KeyReaderImpl from '../../impl/KeyReaderImpl';

const TEST_DATA_DIR = '../../../../../testdata/';
const CERT_FILE = 'js_certops_test_cert.pem';
const PRIVATE_KEY_FILE = 'js_certops_test_private_key.pem';
const PUBLIC_KEY_FILE = 'js_certops_test_public_key.pem';
const CSR_FILE = 'js_certops_test_csr.pem';

const CERT_CA = 'cert-csr/cert_ca.pem';
const CERT_ROOT_CA = 'cert-csr/cert_ca_root.pem';
const CERT_SIGNED = 'cert-csr/cert_signed.pem';

const params: CryptoParams = CryptoConfig.createDefaultParams();
let kg: KeyGenerator;
let kp: CryptoKeyPair;
let kr = new KeyReaderImpl();

const writeToFile = (data: string, fileName: string) => {
  const folderPath = `${__dirname}/${TEST_DATA_DIR}`;
  const filePath = `${folderPath}${fileName}`;
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
  }
  fs.writeFileSync(filePath, data, { encoding: 'binary', flag: 'w' });
};

const readFromFile = (fileName: string, size?: number) => {
  const file = fs.readFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, 'binary');
  if (size && size < file.length) {
    return file.substr(0, size);
  }
  return file;
};

const fillProperties = (): SubjectProperties => {
  return {
    CN: 'al@cn.ua',
    O: 'FirstBridge',
    OU: 'FB-cn',
    L: 'Chernigiv',
    C: 'UA',
    E: 'a.lukin@gmail.com',
    SERIALNUMBER: '1234567890',
    UID: '1234567890',
    BusinessCategory: '1234567890abcdef',
    TelephoneNumber: '380953610821',
  };
};

describe('X509 Certificate Operations', () => {
  beforeAll(async () => {
    kg = new KeyGeneratorEC(params);
    kp = await kg.generateKeys();
  });

  test('Create SelfSignedX509v3', async () => {
    const properties: SubjectProperties = fillProperties();
    const certData: CertificateRequestData = CertificateRequestData.fromProperty(
      properties,
      CertificateRequestData.CSRType.PERSON,
    );
    certData.processCertData(true);

    const instance: X509CertOperations = new X509CertOperationsImpl(params);
    const { certificate, certificatePem, privateKeyPem, publicKeyPem } = await instance.createSelfSignedX509v3(
      kp,
      certData,
    );

    const subjectData = CertificateRequestData.getSubjectFromCertificate(certificate);
    expect(subjectData).toMatchObject(properties);

    writeToFile(certificatePem, CERT_FILE);
    writeToFile(privateKeyPem, PRIVATE_KEY_FILE);
    writeToFile(publicKeyPem, PUBLIC_KEY_FILE);
  });

  test('Create X509CertificateRequest', async () => {
    const properties: SubjectProperties = fillProperties();
    const certData: CertificateRequestData = CertificateRequestData.fromProperty(
      properties,
      CertificateRequestData.CSRType.PERSON,
    );
    certData.processCertData(true);
    const challengePassword = '1234567890';

    const instance: X509CertOperations = new X509CertOperationsImpl(params);
    const {
      certificateRequest,
      certificateRequestPem,
      privateKeyPem,
      publicKeyPem,
    } = await instance.createX509CertificateRequest(kp, certData, challengePassword);

    const subjectData = CertificateRequestData.getSubjectFromCertificate(certificateRequest);
    expect(subjectData).toMatchObject(properties);

    writeToFile(certificateRequestPem, CSR_FILE);
    writeToFile(privateKeyPem, PRIVATE_KEY_FILE);
    writeToFile(publicKeyPem, PUBLIC_KEY_FILE);
  });

  test('Verify Certificate', async () => {
    const certRootCA = kr.readX509CertPEMorDER(readFromFile(CERT_ROOT_CA));
    const certCA = kr.readX509CertPEMorDER(readFromFile(CERT_CA));
    const certSigned = kr.readX509CertPEMorDER(readFromFile(CERT_SIGNED));

    const instance: X509CertOperations = new X509CertOperationsImpl(params);
    const result = await instance.verifyCertificate([certRootCA], [certCA, certSigned], []);
    expect(result).toBe(true);
  });
});
