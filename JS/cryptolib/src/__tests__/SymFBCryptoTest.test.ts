import * as fs from 'fs';
import { arrayBufferToString, stringToArrayBuffer } from 'pvutils';
import KeyReaderImpl from '../impl/KeyReaderImpl';
import KeyPair from '../KeyPair';
import SymJCEImpl from '../impl/ecc/SymJCEImpl';
import CryptoConfig from '../CryptoConfig';
import SymCryptor from '../SymCryptor';
import AEADCiphered from '../dataformat/AEADCiphered';
import AEADPlain from '../dataformat/AEADPlain';

const kr = new KeyReaderImpl();
const kpAlice = new KeyPair();
const kpBob = new KeyPair();
const TEST_DATA_DIR = '../../../../testdata/';
const PLAIN_FILE = 'encrypt_sym_test_plain.bin';
const KEY_FILE = 'encrypt_sym_test_key.bin';
const OUT_FILE_ENCRYPT_SYM = 'encrypt_sym_test.bin';
const OUT_FILE_ENCRYPT_SYM_JS = 'js_encrypt_sym_test.bin';
const OUT_FILE_ENCRYPT_SYM_AEAD = 'encrypt_sym_aead_test.bin';
const OUT_FILE_ENCRYPT_SYM_AEAD_JS = 'js_encrypt_sym_aead_test.bin';
const OPEN_TEXT = 'This is test open text. Should be visisble as is';
const RANDOM_BYTES_NUMBER = 4096;

const writeToFile = (data: Uint8Array, fileName: string) => {
  const dataText = arrayBufferToString(data);
  fs.writeFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, dataText, { encoding: 'binary', flag: 'w' });
};

const readFromFile = (fileName: string) => {
  return fs.readFileSync(`${__dirname}/${TEST_DATA_DIR}${fileName}`, 'binary');
};

const params = CryptoConfig.createDefaultParams();
const plainArray: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(PLAIN_FILE)));
const open = OPEN_TEXT;
const openFormatted = new Uint8Array(stringToArrayBuffer(open));
const skb = new Uint8Array(stringToArrayBuffer(readFromFile(KEY_FILE)));

function compareToFile(data: ArrayBuffer | Uint8Array, fileName: string): boolean {
  const bFile = new Uint8Array(stringToArrayBuffer(readFromFile(fileName)));
  const bData = new Uint8Array(data);
  if (bFile.byteLength !== bData.byteLength) return false;
  for (let i = 0; i < bData.byteLength; i++) {
    if (bFile[i] !== bData[i]) return false;
  }
  return true;
}

describe('Symmetric Key Algorithms', () => {
  test('EncryptToFile', async () => {
    expect.assertions(2);
    let key = new Uint8Array(256 / 8);
    let salt = new Uint8Array(4);
    salt = skb.slice(0, salt.byteLength);
    key = skb.slice(salt.byteLength);
    const instanceEnc: SymCryptor = new SymJCEImpl(params);
    instanceEnc.setSalt(salt);
    instanceEnc.setNonce(null); // generate random nonce
    await instanceEnc.setKey(key);
    const encrypted: Uint8Array = await instanceEnc.encrypt(plainArray);
    // write encrypted to file prefixed with explicitNonce
    writeToFile(encrypted, OUT_FILE_ENCRYPT_SYM_JS);

    // 1) JS: read encrypted data file into "encrypted" byte array
    const encryptedJs = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_SYM_JS)));
    // prepare instance for decription.
    const instanceDecJs: SymCryptor = new SymJCEImpl(params);
    instanceDecJs.setSalt(salt);
    await instanceDecJs.setKey(key);
    // There is no need to call setSymmetricNonce() because nonce
    // is part of encrypted data (prefix of 8 bytes length)
    const plainDecryptedJs: Uint8Array = await instanceDecJs.decrypt(encryptedJs);
    // check that decrypted and plain arrays match
    expect(plainDecryptedJs).toEqual(plainArray);

    // 2) Java: read encrypted data file into "encrypted" byte array
    const encryptedJava = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_SYM)));
    // prepare instance for decription.
    const instanceDecJava: SymCryptor = new SymJCEImpl(params);
    instanceDecJava.setSalt(salt);
    await instanceDecJava.setKey(key);
    // There is no need to call setSymmetricNonce() because nonce
    // is part of encrypted data (prefix of 8 bytes length)
    const plainDecryptedJava: Uint8Array = await instanceDecJava.decrypt(encryptedJava);
    // check that decrypted and plain arrays match
    expect(plainDecryptedJava).toEqual(plainArray);
  });

  test('EncryptAEADToFile', async () => {
    expect.assertions(6);
    let key = new Uint8Array(256 / 8);
    let salt = new Uint8Array(4);
    salt = skb.slice(0, salt.byteLength);
    key = skb.slice(salt.byteLength);
    const instanceEnc: SymCryptor = new SymJCEImpl(params);
    instanceEnc.setSalt(salt);
    instanceEnc.setNonce(null); // generate random nonce
    await instanceEnc.setKey(key);
    const encrypted: AEADCiphered = await instanceEnc.encryptWithAEAData(plainArray, openFormatted);
    const wrb: Uint8Array = encrypted.toBytes();
    // write encrypted to file prefixed with explicitNonce
    writeToFile(wrb, OUT_FILE_ENCRYPT_SYM_AEAD_JS);

    // 1) JS: read encrypted data file into "encryptedBuf" byte array
    const encryptedBuf: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_SYM_AEAD_JS)));
    const instanceDec: SymCryptor = new SymJCEImpl(params);
    instanceDec.setSalt(salt);
    await instanceDec.setKey(key);
    const decrypted: AEADPlain = await instanceDec.decryptWithAEAData(encryptedBuf);
    // check
    expect(decrypted.hmacOk).toBe(true);
    expect(decrypted.decrypted).toEqual(plainArray);
    expect(decrypted.plain).toEqual(openFormatted);

    // 2) Java: read encrypted data file into "encryptedBuf" byte array
    const encryptedBufJava: Uint8Array = new Uint8Array(stringToArrayBuffer(readFromFile(OUT_FILE_ENCRYPT_SYM_AEAD)));
    const instanceDecJava: SymCryptor = new SymJCEImpl(params);
    instanceDecJava.setSalt(salt);
    await instanceDecJava.setKey(key);
    const decryptedJava: AEADPlain = await instanceDecJava.decryptWithAEAData(encryptedBufJava);
    // check
    expect(decryptedJava.hmacOk).toBe(true);
    expect(decryptedJava.decrypted).toEqual(plainArray);
    expect(decryptedJava.plain).toEqual(openFormatted);
  });
});
