/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

const { Crypto } = require('node-webcrypto-ossl');
const crypto = new Crypto();

/**
 * Key holder class for asymmetric crypto operations
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class AsymKeysHolder {
  private _privateKey: CryptoKey;
  private _ourPublicKey: CryptoKey;
  private _theirPublicKey: CryptoKey;

  /**
   * Sets asymmetric keys
   *
   * @param ourPubkey public key from our key pair
   * @param privKey private key from out key pair
   * @param theirPubKey public key of remote party
   */
  public constructor(ourPubkey: CryptoKey, privKey: CryptoKey, theirPubKey: CryptoKey) {
    this._privateKey = privKey;
    this._ourPublicKey = ourPubkey;
    this._theirPublicKey = theirPubKey;
  }

  public async getEncoded(key: CryptoKey): Promise<ArrayBuffer> {
    try {
      return await crypto.subtle.exportKey('spki', key);
    } catch (ex) {
      throw new Error(`Error during exporting of key: ${ex}`);
    }
  }

  /**
   * Key pair with our (PublicKey, PrivateKey)
   * @param keyPair public and private keys of "our side"
   */
  public setOurKeyPair(keyPair: CryptoKeyPair): void {
    this._privateKey = keyPair.privateKey;
    this._ourPublicKey = keyPair.publicKey;
  }

  get theirPublicKey(): CryptoKey {
    return this._theirPublicKey;
  }

  set theirPublicKey(value: CryptoKey) {
    this._theirPublicKey = value;
  }

  get ourPublicKey(): CryptoKey {
    return this._ourPublicKey;
  }

  set ourPublicKey(value: CryptoKey) {
    this._ourPublicKey = value;
  }

  get privateKey(): CryptoKey {
    return this._privateKey;
  }

  set privateKey(value: CryptoKey) {
    this._privateKey = value;
  }
}
