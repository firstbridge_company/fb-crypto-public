/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import DataRecord from './DataRecord';

/**
 * JSON FbWallet model
 */
export default class FbWalletModel {
  public version: string = '1.0.0';
  public name: string = 'noName';
  public keys: KeyRecord[] = [];
  public data: DataRecord[] = [];
}
