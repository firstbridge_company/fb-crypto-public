/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import * as _ from 'lodash';
import CryptoConfig from '../CryptoConfig';
import CryptoFactory from '../CryptoFactory';
import CryptoParams from '../CryptoParams';
import SymCryptor from '../SymCryptor';
import AEADCiphered from '../dataformat/AEADCiphered';

/**
 * Binary encrypted container for arbitrary data. It contains open data as AAD
 * and open data could be extracted without proper encryption key. Protected
 * data are gziped and then encrypted with AES256. So it makes extremely
 * difficult or impossible to extract protected data without proper key.
 */
export default class FbCryptoContainer {
  public static FILE_MAGIC: Uint8Array = new Uint8Array([0x0f, 0x0b, 0x0c, 0x69]);
  public static MAGIC_SIZE: number = 4;
  public static SALT_SIZE: number = 4; // IV=salt+nonce
  public static IV_SIZE: number = 12; // 4 bytes of salt and 8 bytes of explicit nonce in AEADMEssage
  public static BUF_SIZE: number = 4096;
  public static MAX_SIZE: number = FbCryptoContainer.BUF_SIZE * 1024 * 8; // 32M
  private _openData!: Uint8Array;
  private _IV!: Uint8Array;

  params: CryptoParams = CryptoConfig.createSecp521r1();
  private cryptoFactory: CryptoFactory = CryptoFactory.newInstance(this.params);

  /**
   * Sets open data for container
   *
   * @param value
   */
  set openData(value: Uint8Array) {
    this._openData = value;
  }

  /**
   * gets open data from container. It is enough to just call read() and ignore
   * exceptions.
   *
   * @return open data of container
   */
  get openData(): Uint8Array {
    return this._openData;
  }

  set IV(value: Uint8Array) {
    this._IV = value;
  }

  /**
   * All 12 bytes of IV. Note that is is ready only after read() or readOpenDataOnly()
   * @return 12 bytes of IV
   */
  get IV(): Uint8Array {
    return this._IV;
  }

  private static arrayCopy(src: Uint8Array, srcPos: number, dst: Uint8Array, dstPos: number, length: number): void {
    Object.assign(dst, Array(dstPos).concat(src.slice(srcPos, srcPos + length)));
  }

  private composeIV(salt: Uint8Array, nonce: Uint8Array): void {
    this.IV = new Uint8Array(FbCryptoContainer.IV_SIZE);
    FbCryptoContainer.arrayCopy(salt, 0, this.IV, 0, 4);
    FbCryptoContainer.arrayCopy(nonce, 0, this.IV, 4, 8);
  }

  /**
   * Reads open and protected data from container. If key is null or wrong,
   * open data is set anyway, just ignore exceptions. If key is correct,
   * protected data are extracted and open data are verified.
   *
   * @param is input stream
   * @param key 256 bit key
   * @return array of protected data stored in container
   * @throws IOException
   * @throws CryptoNotValidException
   */
  public read(is: ReadableStream, key: Uint8Array): Uint8Array {
    const magic: Uint8Array = new Uint8Array(FbCryptoContainer.MAGIC_SIZE);
    // is.read(magic); // TODO fix
    const salt: Uint8Array = new Uint8Array(FbCryptoContainer.SALT_SIZE);
    // is.read(salt); // TODO fix
    if (_.isEqual(magic, FbCryptoContainer.FILE_MAGIC)) {
      const data = new Uint8Array(FbCryptoContainer.MAX_SIZE);
      // let r: number;
      // let sz: number = 0;
      // while ((r = is.read(data, sz, FbCryptoContainer.MAX_SIZE - sz)) >= 0) { // TODO fix
      //     sz = sz + r;
      //     if (sz > FbCryptoContainer.MAX_SIZE) {
      //         throw new CryptoNotValidException("Maximum size of container excided");
      //     }
      // }
      // return this.decrypt(Object.assign(new Uint8Array(), data).slice(sz - 1), key, salt);
      return new Uint8Array();
    } else {
      throw new Error('Format error, magic at beginnin does not  match');
    }
  }

  /**
   * Reads open data only from container
   *
   * @param is input stream
   * @return open data as byte array
   * @throws IOException
   * @throws CryptoNotValidException
   */
  public readOpenDataOnly(is: any): Uint8Array {
    const magic: Uint8Array = new Uint8Array(FbCryptoContainer.MAGIC_SIZE);
    is.read(magic); // TODO fix
    const salt: Uint8Array = new Uint8Array(FbCryptoContainer.SALT_SIZE);
    is.read(salt); // TODO fix
    if (_.isEqual(magic, FbCryptoContainer.FILE_MAGIC)) {
      const data = new Uint8Array(FbCryptoContainer.MAX_SIZE);
      // let r: number;
      // let sz: number = 0;
      // while ((r = is.read(data, sz, FbCryptoContainer.MAX_SIZE - sz)) >= 0) { // TODO fix
      //     sz = sz + r;
      //     if (sz > FbCryptoContainer.MAX_SIZE) {
      //         throw new CryptoNotValidException("Maximum size of container excided");
      //     }
      // }
      // const msg: AEADMessage = AEADMessage.fromBytes(Object.assign(new Uint8Array(), data).slice(sz - 1), this.params);
      // this.composeIV(salt, msg.getExplicitNonce());
      // this.openData = msg.aatext;
      return this.openData;
    } else {
      throw new Error('Format error, magic at beginnin does not  match');
    }
  }

  private decrypt(input: Uint8Array, key: Uint8Array, salt: Uint8Array): Uint8Array {
    const msg: AEADCiphered = AEADCiphered.fromBytes(input, this.params);
    this.openData = msg.aatext;
    this.composeIV(salt, msg.getExplicitNonce());
    const symCrypto: SymCryptor = this.cryptoFactory.getSymCryptor();
    symCrypto.setSalt(salt);
    symCrypto.setKey(key);
    const a: any = symCrypto.decryptWithAEAData(input); // TODO type AEAD
    const uncomp: Uint8Array = this.gzipUncompress(a.decrypted);
    this.openData = Object.assign(new Uint8Array(), a.plain).slice(a.plain.length);
    return uncomp;
  }

  private async encrypt(input: Uint8Array, key: Uint8Array, IV: Uint8Array): Promise<Uint8Array> {
    const comp: Uint8Array = this.gzipCompress(input);
    const symCrypto: SymCryptor = this.cryptoFactory.getSymCryptor();
    symCrypto.setIV(IV);
    symCrypto.setKey(key);
    const am: AEADCiphered = await symCrypto.encryptWithAEAData(comp, this.openData);
    return am.toBytes();
  }

  /**
   * Saves open data (if set) and protected data in ecnrypted container.
   *
   * @param os output stream
   * @param plain protected data
   * @param key 256 bit (32 bytes) key
   * @param IV 12 bytes of initialization vector;
   * @throws IOException
   * @throws CryptoNotValidException
   */
  public async save(os: any, plain: Uint8Array, key: Uint8Array, IV: Uint8Array): Promise<void> {
    os.write(FbCryptoContainer.FILE_MAGIC);
    os.write(IV, 0, 4); // first 4 bytes is salt, last 8 is nonce
    const res: Uint8Array = await this.encrypt(plain, key, IV);
    os.write(res);
  }

  /**
   * compresses data with GZIP
   *
   * @param uncompressedData plain data
   * @return compressed data
   * @throws IOException
   */
  public gzipCompress(uncompressedData: Uint8Array): Uint8Array {
    // ByteArrayOutputStream bos = new ByteArrayOutputStream(uncompressedData.length);
    // GZIPOutputStream gzipOS = new GZIPOutputStream(bos);
    // gzipOS.write(uncompressedData);
    // gzipOS.close();
    // byte[] result = bos.toByteArray();
    // return result;
    return new Uint8Array();
  }

  /**
   * uncompresses GZIP data
   *
   * @param compressedData gzip compressed data
   * @return plain uncompressed data
   * @throws IOException
   */
  public gzipUncompress(compressedData: Uint8Array): Uint8Array {
    // ByteArrayInputStream bis = new ByteArrayInputStream(compressedData);
    // ByteArrayOutputStream bos = new ByteArrayOutputStream();
    // GZIPInputStream gzipIS = new GZIPInputStream(bis);
    // byte[] buffer = new byte[1024];
    // int len;
    // while ((len = gzipIS.read(buffer)) != -1) {
    //     bos.write(buffer, 0, len);
    // }
    // byte[] result = bos.toByteArray();
    // return result;
    return new Uint8Array();
  }
}
