/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import * as _ from 'lodash';
import FbWalletModel from './FbWalletModel';
import DataRecord from './DataRecord';

/**
 * JSON-based encrypted general purpose wallet
 */
export default class FbWallet {
  private _wallet: FbWalletModel;

  public constructor() {
    this._wallet = new FbWalletModel();
  }

  public addData(dr: DataRecord): void {
    this._wallet.data.push(dr);
  }

  public addKey(kr: KeyRecord): void {
    this._wallet.keys.push(kr);
  }

  public getData(alias: string): DataRecord | null {
    let res: DataRecord | null = null;
    for (const dr of this.wallet.data) {
      if (_.isEqual(alias, dr.alias)) {
        res = dr;
        break;
      }
    }
    return res;
  }

  public getKeys(alias: string): KeyRecord | null {
    let res: KeyRecord | null = null;
    for (const dr of this.wallet.keys) {
      if (_.isEqual(alias, dr.alias)) {
        res = dr;
        break;
      }
    }
    return res;
  }

  public getAllKeys(): KeyRecord[] {
    return this._wallet.keys;
  }

  public getAllData(): DataRecord[] {
    return this._wallet.data;
  }

  get wallet(): FbWalletModel {
    return this._wallet;
  }

  set wallet(value: FbWalletModel) {
    this._wallet = value;
  }
}
