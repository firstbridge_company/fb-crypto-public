/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import FbCryptoContainer from './FbCryptoContainer';
import CryptoFactory from '../CryptoFactory';
import Digester from '../Digester';
import CryptoConfig from '../CryptoConfig';

/**
 * JSON-based encrypted general purpose wallet
 *
 * @param <T> Model ow wallet
 */
export default class GenericWallet<T> {
  // private mapper: ObjectMapper = new ObjectMapper();
  private _wallet: T | null = null;
  private _openData!: Uint8Array;
  private _containerIv!: Uint8Array;

  // Class walletModelClass;

  // get wallet(): T | null {
  //     return this._wallet;
  // }
  //
  // set wallet(value: T) {
  //     this._wallet = value;
  // }

  /**
   *  Gets open data of wallet even if key is wrong
   * @return
   */
  get openData(): Uint8Array {
    return this._openData;
  }

  /**
   * Sets open data for wallet
   * @param value
   */
  set openData(value: Uint8Array) {
    this._openData = value;
  }

  get containerIv(): Uint8Array {
    return this._containerIv;
  }

  set containerIv(value: Uint8Array) {
    this._containerIv = value;
  }

  // public GenericWallet(Class walletModelClass) {
  //     this.walletModelClass=walletModelClass;
  // }

  public openFile(path: string, key: Uint8Array): void {
    // try(FileInputStream fis = new FileInputStream(path)) {
    //     openStream(fis, key);
    // }
  }

  /**
   * Get only open data.
   * @param path
   * @throws FileNotFoundException
   * @throws IOException
   * @throws CryptoNotValidException
   */
  public readOpenData(path: string): void {
    // try(FileInputStream fis = new FileInputStream(path)) {
    //     FbCryptoContainer c = new FbCryptoContainer();
    //     this.openData = c.readOpenDataOnly(fis);
    //     this.container_iv = c.IV;
    // }
  }

  /**
   * Get only open data.
   * @throws FileNotFoundException
   * @throws IOException
   * @throws CryptoNotValidException
   */
  // public readOpenData(is: any): void {
  // try {
  //     FbCryptoContainer c = new FbCryptoContainer();
  //     this.openData = c.readOpenDataOnly(is);
  //     this.container_iv = c.getFullIV();
  // } catch (Exception ex){
  //     throw ex;
  // } finally {
  //     is.close();
  // }
  // }

  public openStream(is: any, key: Uint8Array): void {
    const c: FbCryptoContainer = new FbCryptoContainer();
    try {
      const data: Uint8Array = c.read(is, key);
      this.openData = c.openData;
      this.containerIv = c.IV;
      //     wallet = (T)mapper.readValue(data, walletModelClass);
    } catch (e) {
      // try to read open data anyway and re-throw
      this.openData = c.openData;
      throw e;
    }
  }

  public saveFile(path: string, key: Uint8Array, IV: Uint8Array): void {
    // try(FileOutputStream fos = new FileOutputStream(path)) {
    //     saveStream(fos, key, IV);
    // }
  }

  public saveStream(os: any, key: Uint8Array, IV: Uint8Array): void {
    const c: FbCryptoContainer = new FbCryptoContainer();
    c.openData = this.openData;
    // c.save(os, mapper.writeValueAsBytes(wallet), key, IV);
  }

  public keyFromPassPhrase(passPhrase: string, salt: Uint8Array): Uint8Array {
    const f: CryptoFactory = CryptoFactory.newInstance(CryptoConfig.createDefaultParams());
    const d: Digester = f.getDigesters();
    return d.PBKDF2(passPhrase, salt);
  }
}
