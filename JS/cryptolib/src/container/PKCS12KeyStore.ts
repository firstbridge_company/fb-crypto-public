/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import { Certificate } from 'tls';

/**
 * Java key store is actually PKCS12 key store. So this class supports p12 and
 * jks files
 */
export default class PKCS12KeyStore {
  public static KEYSTORE_TYPE: string = 'pkcs12';
  private keystore: any;
  private _aliases: string[] = [];
  private _certificates: Certificate[] = [];

  // private static log = LoggerFactory.getLogger(PKCS12KeyStore.class);

  public openKeyStore(path: string, password: string): boolean {
    // let res: boolean = true;
    // let is: any = null;
    // try {
    //     File file = new File(path);
    //     is = new FileInputStream(file);
    //     keystore = KeyStore.getInstance(KEYSTORE_TYPE,FBCryptoParams.getProvider());
    //     keystore.load(is, password.toCharArray());
    //     Enumeration<String> enumeration = keystore.aliases();
    //     while (enumeration.hasMoreElements()) {
    //         String alias = enumeration.nextElement();
    //         aliases.add(alias);
    //         Certificate certificate = keystore.getCertificate(alias);
    //         certificates.add(certificate);
    //     }
    // } catch (FileNotFoundException ex) {
    //     log.error("File" + path + " does not exists", ex);
    //     res = false;
    // } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
    //     log.error("File" + path + " is not loadable", ex);
    //     res = false;
    // } finally {
    //     try {
    //         if (is != null) {
    //             is.close();
    //         }
    //     } catch (IOException ex) {
    //         log.error("File" + path + " is not loadable", ex);
    //         res = false;
    //     }
    // }
    // return res;
    return true;
  }

  public createOrOpenKeyStore(path: string, password: string): boolean {
    let res: boolean = true;
    try {
      // const file: File = new File(path);
      // const keystore = KeyStore.getInstance(PKCS12KeyStore.KEYSTORE_TYPE, FBCryptoParams.getProvider());
      // if (file.exists()) {
      //     // if exists, load
      //     res = this.openKeyStore(path, password);
      // } else {
      //     // if not exists, create
      //     keystore.load(null, null);
      //     keystore.store(new FileOutputStream(file), password.toCharArray());
      // }
    } catch (ex) {
      // log.error("Can not create file" + path, ex);
      res = false;
    }
    return res;
  }

  get aliases(): string[] {
    return this._aliases;
  }

  get certificates(): Certificate[] {
    return this._certificates;
  }

  public getKey(alias: string, password: string): any {
    let key = null;
    try {
      key = this.keystore.getKey(alias, password.split('').join(','));
    } catch (ex) {
      // log.error("Can not read key with alias:" + alias, ex);
    }
    return key;
  }

  public getPrivateKey(alias: string, password: string): any {
    // PrivateKey key = null;
    // Key k = getKey(alias, password);
    // if(k instanceof PrivateKey){
    //     key=(PrivateKey) k;
    // }
    // return key;
  }

  public addSymmetricKey(key: Uint8Array, algo: string, alias: string, password: string): boolean {
    // boolean res = true;
    // try {
    //     SecretKey secretKey = new SecretKeySpec(key,algo);
    //     KeyStore.SecretKeyEntry secret = new KeyStore.SecretKeyEntry(secretKey);
    //     KeyStore.ProtectionParameter pwd  = new KeyStore.PasswordProtection(password.toCharArray());
    //     keystore.setEntry(alias, secret, pwd);
    //     return res;
    // } catch (KeyStoreException ex) {
    //    log.error("Can not set key entry with alias: "+alias,ex);
    //    res=false;
    // }
    // return res;
    return true;
  }

  public addCertificate(alias: string, cert: any): boolean {
    // boolean res = true;
    // try {
    //     keystore.setCertificateEntry(alias, cert);
    // } catch (KeyStoreException ex) {
    //    log.error("Can not set certificate entry with alias: "+alias,ex);
    //    res=false;
    // }
    // return res;
    return true;
  }

  public addPrivateKey(pvtKey: any, alias: string, password: string, cert: any, caCert: any): boolean {
    // boolean res = true;
    // if(password==null){
    //     password="";
    // }
    // try {
    //     X509Certificate[] chain = new X509Certificate[2];
    //     chain[0] = cert;
    //     chain[1] = caCert;
    //     keystore.setKeyEntry(alias, pvtKey, password.toCharArray(), chain);
    //     return res;
    // } catch (KeyStoreException ex) {
    //     log.error("Can not set private key entry with alias: "+alias,ex);
    //     res=false;
    // }
    // return res;
    return true;
  }

  public save(path: string, password: string): boolean {
    // boolean res = false;
    // File file = new File(path);
    // try {
    //     keystore.store(new FileOutputStream(file), password.toCharArray());
    // } catch ( KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
    //      log.error("Can not dave keystore to file:" + file.getAbsolutePath(), ex);
    // }
    // return res;
    return true;
  }
}
