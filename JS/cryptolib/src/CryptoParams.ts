/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Configuration parameters for all FBCrypto library
 *
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class CryptoParams {
  public static KEY_DERIVATION_FN: string = 'PBKDF2WithHmacSHA256'; // produces 256 bit key
  public static PBKDF2_ITERATIONS: number = 16;
  public static PBKDF2_KEYELEN: number = 256; // 256 bits for AES

  public static GCM_AUTH_TAG_LEN_BITS: number = 128; // 128 bits
  public static AES_IV_LEN: number = 12; // 12 bytes

  protected _baseKeyLen!: number;
  protected _signatureSchema!: string;
  protected _defaultCurve!: string;
  protected _symCipher!: string;
  protected _asymCipher!: string;
  // for RSA it is just RSA, for EC is depends
  protected _asymIesCipher!: string;
  protected _digester!: string;
  protected _signatureAlgorythm!: string;
  protected _keyDerivationFn!: string;
  protected _pbkdf2Iterations!: number;
  protected _gcmAuthTagLenBits!: number;
  protected _symIvLen!: number;
  protected _iesIvLen!: number;
  protected _symKeyLen!: number;
  protected _symGcmSaltLen!: number;
  protected _symGcmNonceLen!: number;
  protected _keyAgreementDigester!: string;

  constructor(
    baseKeyLen: number,
    signatureSchema: string,
    defaultCurve: string,
    symCipher: string,
    asymCipher: string,
    asymIesCipher: string,
    digester: string,
    signatureAlgorythm: string,
    keyDerivationFn: string,
    pbkdf2Iterations: number,
    gcmAuthTagLenBits: number,
    aesIvLen: number,
    iesIvLen: number,
    aesKeyLen: number,
    aesGcmSaltLen: number,
    aesGcmNonceLen: number,
    keyAgreementDigester: string,
  ) {
    this._baseKeyLen = baseKeyLen;
    this._signatureSchema = signatureSchema;
    this._defaultCurve = defaultCurve;
    this._symCipher = symCipher;
    this._asymCipher = asymCipher;
    this._asymIesCipher = asymIesCipher;
    this._digester = digester;
    this._signatureAlgorythm = signatureAlgorythm;
    this._keyDerivationFn = keyDerivationFn;
    this._pbkdf2Iterations = pbkdf2Iterations;
    this._gcmAuthTagLenBits = gcmAuthTagLenBits;
    this._symIvLen = aesIvLen;
    this._iesIvLen = iesIvLen;
    this._symKeyLen = aesKeyLen;
    this._symGcmSaltLen = aesGcmSaltLen;
    this._symGcmNonceLen = aesGcmNonceLen;
    this._keyAgreementDigester = keyAgreementDigester;
  }

  public toString() {
    const res =
      'baseKeyLen: ' +
      this.baseKeyLen +
      '\n' +
      'signatureSchema: ' +
      this.signatureSchema +
      '\n' +
      'defaultCurve: ' +
      this.defaultCurve +
      '\n' +
      'symCipher: ' +
      this.symCipher +
      '\n' +
      'asymCipher: ' +
      this.asymCipher +
      '\n' +
      'asymIesCipher: ' +
      this.asymCipher +
      '\n' +
      'digester: ' +
      this.digester +
      '\n' +
      'signatureAlgorythm: ' +
      this.signatureAlgorythm +
      '\n' +
      'keyDerivationFn: ' +
      this.keyDerivationFn +
      '\n' +
      'pbkdf2Iterations: ' +
      this.pbkdf2Iterations +
      '\n' +
      'gcmAuthTagLenBits: ' +
      this.gcmAuthTagLenBits +
      '\n' +
      'symIvLen: ' +
      this.symIvLen +
      '\n' +
      'iesIvLen: ' +
      this.iesIvLen +
      '\n' +
      'symKeyLen: ' +
      this.symKeyLen +
      '\n' +
      'symGcmSaltLen: ' +
      this.symGcmSaltLen +
      '\n' +
      'symGcmNonceLen: ' +
      this.symGcmNonceLen +
      '\n' +
      'keyAgreementDigester: ' +
      this.keyAgreementDigester;
    return res;
  }

  public build(): CryptoParams {
    return new CryptoParams(
      this.baseKeyLen,
      this.signatureSchema,
      this.defaultCurve,
      this.symCipher,
      this.asymCipher,
      this.asymIesCipher,
      this.digester,
      this.signatureAlgorythm,
      this.keyDerivationFn,
      this.pbkdf2Iterations,
      this.gcmAuthTagLenBits,
      this.symIvLen,
      this.iesIvLen,
      this.symKeyLen,
      this.symGcmSaltLen,
      this.symGcmNonceLen,
      this.keyAgreementDigester,
    );
  }

  get baseKeyLen(): number {
    return this._baseKeyLen;
  }

  set baseKeyLen(value: number) {
    this._baseKeyLen = value;
  }

  get signatureSchema(): string {
    return this._signatureSchema;
  }

  set signatureSchema(value: string) {
    this._signatureSchema = value;
  }

  get defaultCurve(): string {
    return this._defaultCurve;
  }

  set defaultCurve(value: string) {
    this._defaultCurve = value;
  }

  get symCipher(): string {
    return this._symCipher;
  }

  set symCipher(value: string) {
    this._symCipher = value;
  }

  get asymCipher(): string {
    return this._asymCipher;
  }

  set asymCipher(value: string) {
    this._asymCipher = value;
  }

  get asymIesCipher(): string {
    return this._asymIesCipher;
  }

  set asymIesCipher(value: string) {
    this._asymIesCipher = value;
  }

  get digester(): string {
    return this._digester;
  }

  set digester(value: string) {
    this._digester = value;
  }

  get signatureAlgorythm(): string {
    return this._signatureAlgorythm;
  }

  set signatureAlgorythm(value: string) {
    this._signatureAlgorythm = value;
  }

  get keyDerivationFn(): string {
    return this._keyDerivationFn;
  }

  set keyDerivationFn(value: string) {
    this._keyDerivationFn = value;
  }

  get pbkdf2Iterations(): number {
    return this._pbkdf2Iterations;
  }

  set pbkdf2Iterations(value: number) {
    this._pbkdf2Iterations = value;
  }

  get gcmAuthTagLenBits(): number {
    return this._gcmAuthTagLenBits;
  }

  set gcmAuthTagLenBits(value: number) {
    this._gcmAuthTagLenBits = value;
  }

  get symIvLen(): number {
    return this._symIvLen;
  }

  set symIvLen(value: number) {
    this._symIvLen = value;
  }

  get iesIvLen(): number {
    return this._iesIvLen;
  }

  set iesIvLen(value: number) {
    this._iesIvLen = value;
  }

  get symKeyLen(): number {
    return this._symKeyLen;
  }

  set symKeyLen(value: number) {
    this._symKeyLen = value;
  }

  get symGcmSaltLen(): number {
    return this._symGcmSaltLen;
  }

  set symGcmSaltLen(value: number) {
    this._symGcmSaltLen = value;
  }

  get symGcmNonceLen(): number {
    return this._symGcmNonceLen;
  }

  set symGcmNonceLen(value: number) {
    this._symGcmNonceLen = value;
  }

  get keyAgreementDigester(): string {
    return this._keyAgreementDigester;
  }

  set keyAgreementDigester(value: string) {
    this._keyAgreementDigester = value;
  }
}
