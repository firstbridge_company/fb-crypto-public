/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * Create key pair of public and private keys
 */
export default class KeyPair {
  private _publicKey!: CryptoKey;
  private _privateKey!: CryptoKey;

  get privateKey(): CryptoKey {
    return this._privateKey;
  }

  set privateKey(value: CryptoKey) {
    this._privateKey = value;
  }

  get publicKey(): CryptoKey {
    return this._publicKey;
  }

  set publicKey(value: CryptoKey) {
    this._publicKey = value;
  }
}
