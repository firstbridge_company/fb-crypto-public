/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

/**
 * El Gamal encryption keys holder
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class ElGamalKeyPair {
  private _publicKey!: any;
  private _privateKey!: BigInteger;

  get privateKey(): BigInteger {
    return this._privateKey;
  }

  set privateKey(value: BigInteger) {
    this._privateKey = value;
  }

  get publicKey(): any {
    return this._publicKey;
  }

  set publicKey(value: any) {
    this._publicKey = value;
  }

  public getPrivateKeyX(): BigInteger {
    return this._publicKey.getAffineXCoord().toBigInteger();
  }

  public getPrivateKeyY(): BigInteger {
    return this._publicKey.getAffineYCoord().toBigInteger();
  }
}
