/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

import CryptoParams from './CryptoParams';
import CryptoParamsBuilder from './CryptoParamsBuilder';

/**
 * Configuration parameters for all FBCrypto library
 * @author Ellina Kolisnichenko ellina.kolisnichenko@firstbridge.io
 */
export default class CryptoConfig {
  protected provider!: any;

  public static createDefaultParams(): CryptoParams {
    return this.createSecp521r1();
  }

  public static createSecp521r1(): CryptoParams {
    const builder: CryptoParamsBuilder = new CryptoParamsBuilder();

    builder.signatureSchema = 'EC'; // EC only for Oracle provider;
    builder.baseKeyLen = 521;
    builder.defaultCurve = 'secp521r1';
    builder.symCipher = 'AES/GCM/NoPadding';
    builder.asymCipher = 'AES/GCM/NoPadding';
    builder.asymIesCipher = 'ECIESwithAES-CBC';
    builder.digester = 'SHA-512';
    builder.signatureAlgorythm = 'SHA512withECDSA';
    builder.keyDerivationFn = 'PBKDF2WithHmacSHA256'; // produces 256 bit key
    builder.pbkdf2Iterations = 16;
    builder.gcmAuthTagLenBits = 128; // 128 bits
    builder.symIvLen = 12; // 12 bytes
    builder.iesIvLen = 16; // 16 bytes
    builder.symKeyLen = 256 / 8; // 32 bytes
    builder.symGcmSaltLen = 4; // 4 of 12 bytes
    builder.symGcmNonceLen = 8; // 8 of 12 bytes
    builder.keyAgreementDigester = 'SHA-256';
    return builder.build();
  }

  public static createRSAn(keylen: number): CryptoParams {
    if (keylen < 2048 || keylen > 16048) {
      throw new Error('Unsupported key length: ' + keylen);
    }

    const builder: CryptoParamsBuilder = new CryptoParamsBuilder();

    builder.signatureSchema = 'RSA'; // EC only for Oracle provider;
    builder.defaultCurve = '';
    builder.symCipher = 'AES/GCM/NoPadding';
    builder.asymCipher = 'AES/GCM/NoPadding';
    builder.asymIesCipher = 'RSA/ECB/PKCS1Padding';
    builder.keyDerivationFn = 'PBKDF2WithHmacSHA256'; // produces 256 bit key
    builder.pbkdf2Iterations = 16;
    builder.gcmAuthTagLenBits = 128; // 128 bits
    builder.symIvLen = 12; // 12 bytes
    builder.iesIvLen = 16; // 16 bytes
    builder.symKeyLen = 256 / 8; // 32 bytes
    builder.symGcmSaltLen = 4; // 4 of 12 bytes
    builder.symGcmNonceLen = 8; // 8 of 12 bytes
    builder.keyAgreementDigester = 'SHA-256';
    if (keylen === 2048) {
      builder.baseKeyLen = 2048;
      builder.digester = 'SHA-256';
      builder.signatureAlgorythm = 'SHA256withRSA';
    } else if (keylen <= 4096) {
      builder.baseKeyLen = 4096;
      builder.digester = 'SHA-384';
      builder.signatureAlgorythm = 'SHA384withRSA';
    } else if (keylen <= 8192) {
      // > 4K means 8K
      builder.baseKeyLen = 8192;
      builder.digester = 'SHA-512';
      builder.signatureAlgorythm = 'SHA512withRSA';
    } else if (keylen <= 16384) {
      // > 8K means 16K
      builder.baseKeyLen = 16384;
      builder.digester = 'SHA-512';
      builder.signatureAlgorythm = 'SHA512withRSA';
    }
    return builder.build();
  }

  public static createSecp256k1(): CryptoParams {
    const builder: CryptoParamsBuilder = new CryptoParamsBuilder();

    builder.signatureSchema = 'EC'; // EC only for Oracle provider;
    builder.baseKeyLen = 256;
    builder.defaultCurve = 'secp256k1';
    builder.symCipher = 'AES/GCM/NoPadding';
    builder.asymCipher = 'AES/GCM/NoPadding';
    builder.asymIesCipher = 'ECIESwithAES-CBC';
    builder.digester = 'SHA-256';
    builder.signatureAlgorythm = 'SHA256withECDSA';
    builder.keyDerivationFn = 'PBKDF2WithHmacSHA256'; // produces 256 bit key
    builder.pbkdf2Iterations = 16;
    builder.gcmAuthTagLenBits = 128; // 128 bits
    builder.symIvLen = 12; // 12 bytes
    builder.iesIvLen = 16; // 16 bytes
    builder.symKeyLen = 128 / 8; // 32 bytes
    builder.symGcmSaltLen = 4; // 4 of 12 bytes
    builder.symGcmNonceLen = 8; // 8 of 12 bytes
    builder.keyAgreementDigester = 'SHA-256';
    return builder.build();
  }

  public static createPrime256v1(): CryptoParams {
    const builder: CryptoParamsBuilder = new CryptoParamsBuilder();

    builder.signatureSchema = 'EC'; // EC only for Oracle provider;
    builder.baseKeyLen = 256;
    builder.defaultCurve = 'prime256v1';
    builder.symCipher = 'AES/GCM/NoPadding';
    builder.asymCipher = 'AES/GCM/NoPadding';
    builder.asymIesCipher = 'ECIESwithAES-CBC';
    builder.digester = 'SHA-256';
    builder.signatureAlgorythm = 'SHA256withECDSA';
    builder.keyDerivationFn = 'PBKDF2WithHmacSHA256'; // produces 256 bit key
    builder.pbkdf2Iterations = 16;
    builder.gcmAuthTagLenBits = 128; // 128 bits
    builder.symIvLen = 12; // 12 bytes
    builder.iesIvLen = 16; // 16 bytes
    builder.symKeyLen = 128 / 8; // 32 bytes
    builder.symGcmSaltLen = 4; // 4 of 12 bytes
    builder.symGcmNonceLen = 8; // 8 of 12 bytes
    builder.keyAgreementDigester = 'SHA-256';
    return builder.build();
  }
}
