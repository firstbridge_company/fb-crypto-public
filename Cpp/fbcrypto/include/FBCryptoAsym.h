/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */
#pragma once

#include <string>
#include "KeyInterfaces.h"
#include "X509Certificate.h"

class FBCryptoAsym {
public:
    virtual void setAymmetricKeys(PrivateKey& pvtKey, PublicKey& outPubKey, PublicKey& theirPubKey )=0;
};


