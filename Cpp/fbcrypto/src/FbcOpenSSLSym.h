/*
 * Copyright (C) FirstBridge https://firstbridge.io/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For commercial licensing please contact FirstBridge https://firstbridge.io/
 */

#pragma once

#include <vector>

#include "FBCryptoSym.h"

#include "openssl/evp.h"
#include "global.h"

#define SYMMETRIC_KEY_SIZE      32
#define SYMMETRIC_IV_FULL_SIZE  16
#define SYMMETRIC_IV_SIZE       12
#define NONCE_SHIFT              4
#define NONCE_SIZE               4
#define SALT_SYMMETRIC_SIZE      4
#define EXPLICIT_NONCE_SIZE      8




class FbcOpenSSLSym : public FBCryptoSym{
    
public:
    
    FbcOpenSSLSym(FBCryptoParams *params);  
    FbcOpenSSLSym(const FbcOpenSSLSym& orig)=delete;
    ~FbcOpenSSLSym();
    
    void setSymmetricKey( std::vector<Byte> &key  );
    void setSymmetricIV( std::vector<Byte> &IV );
    std::vector<Byte> getSymmetricIV();
    void setSymmetricSalt(std::vector<Byte> &salt );
    void setSymmetricNounce( std::vector<Byte> &explicit_nounce );
    std::vector<Byte> getSymmetricNounce();
    std::vector<Byte> encryptSymmetric( std::vector<Byte> &plain );
    std::vector<Byte> decryptSymmetric( std::vector<Byte> &ciphered );
    AEADMessage encryptSymmetricWithAEAData( std::vector<Byte> &plain, std::vector<Byte> &aeadata );
    AEAD decryptSymmetricWithAEAData( std::vector<Byte> &message );
          
private:
        
    std::vector<Byte> saltSymmetic;
    std::vector<Byte> explicit_nounce_sym;
    std::vector<Byte> prev_explicit_nounce_sym;
    std::vector<Byte> gcmIVsym;
    std::vector<Byte> symmetricKey;
    
    EVP_CIPHER_CTX *ctx;
    const EVP_CIPHER *cipher;
    
    FBCryptoParams *params;    
};
